<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Users extends CI_Controller {
	public function __construct(){
        parent::__construct();
        $this->load->model('Users_model', 'users');
		$this->load->model('Mail', 'mail');
        $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->library('form_validation');
		if ($this->session->userdata('ci_session_key_generate') == FALSE) { 
            redirect('signin');
        }
		if ($this->session->userdata('role') != "administrator") {
			 redirect('profile');
		}
    }
    public function index(){
		$data = array();
        $data['users'] = $this->users->users_list();
		$this->load->view('templates/header', $data);
		$this->load->view('users/index');
		$this->load->view('templates/footer', $data);
    }
	public function register() {
		$this->load->view('templates/header');
		$this->load->view('users/register');
		$this->load->view('templates/footer');
    }
	public function actionCreate() {
        $this->form_validation->set_rules('first_name', 'First Name', 'required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('contact_no', 'Contact No', 'required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]');
        $this->form_validation->set_rules('confirm_password', 'Password Confirmation', 'trim|required|matches[password]');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('company_name', 'Company Name', 'required');
		$this->form_validation->set_rules('country', 'Country', 'required');
		$this->form_validation->set_rules('province', 'Province', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->register();
        } else {
            $firstName = $this->input->post('first_name');
            $lastName = $this->input->post('last_name');
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $contactNo = $this->input->post('contact_no');
            $companyName = $this->input->post('company_name');
            $address = $this->input->post('address');
			$country = $this->input->post('country');
			$province = $this->input->post('province');
            $timeStamp = time();
            $status = 1;
            $verificationCode = 1;
            $verificationLink = site_url() . 'signin?usid=' . urlencode(base64_encode($verificationCode));
			$userName = $this->mail->generateUnique('users', strstr($email, '@', true), 'user_name', NULL, NULL);
			$query = $this->db->get('users');
			$role = 'user';
            $this->users->setUserName($userName);
            $this->users->setFirstName(trim($firstName));
            $this->users->setLastName(trim($lastName));
            $this->users->setEmail($email);
            $this->users->setPassword($password);
            $this->users->setContactNo($contactNo);
            $this->users->setAddress($address);
            $this->users->setCompanyName($companyName);
			$this->users->setCountry($country);
			$this->users->setProvince($province);
            $this->users->setVerificationCode($verificationCode);
            $this->users->setTimeStamp($timeStamp);
            $this->users->setStatus($status);
			$this->users->setRole($role);
            $chk = $this->users->create();
            redirect('users/index');
        }
    }
	public function edit($uid = NULL) {
		//$id = $this->uri->segment(3);
        if (empty($uid)) {
            redirect('users/index');
        } else {
            $data = array();
			$this->users->setUserID($uid);
            $data['userInfo'] = $this->users->getUserDetails();
			$this->load->view('templates/header', $data);
			$this->load->view('users/edit');
			$this->load->view('templates/footer', $data);
        }
    }
    public function editUser() {
        $this->form_validation->set_rules('first_name', 'First Name', 'required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required');
        $this->form_validation->set_rules('contact_no', 'Contact No', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('company_name', 'Company Name', 'required');
		$this->form_validation->set_rules('country', 'Country', 'required');
		$this->form_validation->set_rules('province', 'Province', 'required');
        if ($this->form_validation->run() == FALSE) {
			$uid = $this->input->post('user_id');
            $this->edit($uid);
        } else {
			$uid = $this->input->post('user_id');
            $firstName = $this->input->post('first_name');
            $lastName = $this->input->post('last_name');
            $contactNo = $this->input->post('contact_no');
			$email = $this->input->post('email');
            $companyName = $this->input->post('company_name');
            $address = $this->input->post('address');
			$country = $this->input->post('country');
			$province = $this->input->post('province');
            $timeStamp = time();
			$this->users->setUserID($uid);
            $this->users->setFirstName(trim($firstName));
            $this->users->setLastName(trim($lastName));
            $this->users->setContactNo($contactNo);
            $this->users->setAddress($address);
            $this->users->setCompanyName($companyName);
			$this->users->setCountry($country);
			$this->users->setProvince($province);
            $this->users->setTimeStamp($timeStamp);
            $status = $this->users->update();
            if ($status == TRUE) {
               redirect('users/index');
            }
        }
    }
	public function deleteUser($id = NULL){
		$data = array();
		$didata = array(
            'id' => $this->input->post('id'),
        );
		$uid = $this->input->post('id');
		$this->users->setUserID($uid);
        $vehiclesDetails = $this->users->getVehicleDetails();
		if (!empty($vehiclesDetails)) {
		foreach($vehiclesDetails as $vehiclesDetail):
			$vimags = explode(',',$vehiclesDetail->vehicle_images);
			foreach($vimags as $vimag):
				@unlink('assets/vimages/'.$vimag);
			endforeach;
			$images_zip_name = $vehiclesDetail->images_zip_name;
			@unlink('assets/images_zip/'.$images_zip_name);
			$this->users->setVehicleID($vehiclesDetail->id);
			$this->users->deleteVehicle();
		endforeach;
		}
		$chk = $this->users->deleteUser();
		if ($chk === TRUE) {
			$status = 'ok'; 
		}
		echo $status; die;
    }
	public function deactivate($id = NULL){
		//$id = $this->uri->segment(3); 
		$data = array(
            'status' => 0,
        );
		$this->db->where('id', $id);
		$this->db->update('users', $data);
		redirect('users/index');
    }
	public function activate($id = NULL){
		//$id = $this->uri->segment(3); 
		$data = array(
            'status' => 1,
        );
		$this->db->where('id', $id);
		$this->db->update('users', $data);
		redirect('users/index');
    }
	public function changepwd($id = NULL) {
		//$id = $this->uri->segment(3);
        if (empty($id)){
            redirect('users/index');
        } else {
			$data = array();
            $data['userInfo'] = $id;
			$this->load->view('templates/header');
			$this->load->view('users/changepwd', $data);
			$this->load->view('templates/footer');
        }
    }
	public function actionChangePwd() {
        $this->form_validation->set_rules('change_pwd_password', 'Password', 'trim|required|min_length[8]');
        $this->form_validation->set_rules('change_pwd_confirm_password', 'Password Confirmation', 'trim|required|matches[change_pwd_password]');
        if ($this->form_validation->run() == FALSE) {
            $this->changepwd();
        } else {
            $change_pwd_password = $this->input->post('change_pwd_password');
            $user_id = $this->input->post('user_id');
            $this->users->setUserID($user_id);
            $this->users->setPassword($change_pwd_password);
            $status = $this->users->changePassword();
            if ($status == TRUE) {
                redirect('users/index');
            }
        }
    }
	public function newpwd($id = NULL) {
		$login = site_url() . 'signin';
		//$id = $this->uri->segment(3);
		$this->db->where('id',$id);
		$query = $this->db->get('users');
		$result = $query->result();
		$email = $result[0]->email;
		$this->users->setEmail($email);
		$pass = $this->generateRandomPassword(8);
		$this->users->setPassword($pass);
		$status = $this->users->updateForgotPassword();
		if ($status == TRUE) {
			$this->load->library('encrypt');
			$mailData = array('topMsg' => 'Hi', 'bodyMsg' => 'Your password has been reset successfully!.', 'thanksMsg' => SITE_DELIMETER_MSG, 'delimeter' => SITE_DELIMETER, 'loginLink' => $login, 'pwd' => $pass, 'username' => $email);
			$this->mail->setMailTo($email);
			$this->mail->setMailFrom(MAIL_FROM);
			$this->mail->setMailSubject('Forgot Password!');
			$this->mail->setMailContent($mailData);
			$this->mail->setTemplateName('sendpwd');
			$this->mail->setTemplatePath('mailTemplate/');
			$chkStatus = $this->mail->sendMail(MAILING_SERVICE_PROVIDER);
			if ($chkStatus === TRUE) {
				redirect('users/index');
			} else {
				redirect('users/index');
			}
		} else {
			redirect('users/index');
		}
	}
	public function generateRandomPassword($length = 10) {
        $alphabets = range('a', 'z');
        $numbers = range('0', '9');
        $final_array = array_merge($alphabets, $numbers);
        $password = '';
        while ($length--) {
            $key = array_rand($final_array);
            $password .= $final_array[$key];
        }
        return $password;
    }
}