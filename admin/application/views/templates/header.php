<?php
$sessionArray = $this->session->userdata('ci_seesion_key');
$uid = $sessionArray['user_id'];
?>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width">        
        <title>Rola Motors</title>
        <link rel="shortcut icon" href="<?php print HTTP_IMAGES_PATH; ?>/favicon.ico" />
        <link rel="stylesheet" href="<?php print HTTP_CSS_PATH; ?>bootstrap/bootstrap.min.css">
        <link rel="stylesheet" href="<?php print HTTP_CSS_PATH; ?>bootstrap/bootstrap-theme.min.css">
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,500" rel="stylesheet" type="text/css">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?php print HTTP_CSS_PATH; ?>style.css">
        <link rel="stylesheet" href="<?php print HTTP_CSS_PATH; ?>imgareaselect.css">
        <script type="text/javascript" src="<?php print HTTP_JS_PATH; ?>jquery.min.js"></script>
    </head>
    <body class="page-template-default page page-id-116 logged-in admin-bar  customize-support" cz-shortcut-listen="true">
        <header id="masthead" class="site-header" role="banner">
            <div class="header-top">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <div id="logo">
                                <div class="site-header-inner col-sm-12">
                                    <div class="site-branding">
                                        <h1 class="site-title">
                                            <a href="http://rentapot.co.za/rola/" title="Rola Motors" rel="home">Rola Motors</a>
                                        </h1>
                                    </div>
                                </div>
                            </div>
                        </div><!--.col-sm-3-->
                        <div class="col-sm-6">
                            <div class="header-social-icon-wrap">
                                <ul class="social-icons">
                                </ul>
                            </div><!--.header-social-icon-wrap-->
                        </div><!-- .col-sm-6-->
                    </div>
                </div>
            </div>
            <div id="header-main" class="header-bottom">
                <div class="header-bottom-inner">
                    <div class="container">
                        <div class="row">                 
							<?php if($this->session->userdata('role') == 'administrator'){ ?>
                            <div class="col-sm-12">                        
                                <div class="site-navigation">
                                    <nav class="main-menu" style="display: block;">
                                        <ul id="menu-primari_menu" class="header-nav clearfix"><li id="menu-item-40" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-40"><a href="<?php echo base_url() ?>">Home</a></li>
                                            <li id="menu-item-40" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home  current-menu-item menu-item-40"><a href="<?php echo base_url('users/index') ?>">Users List</a></li>
                                            <li id="menu-item-40" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-40"><a href="<?php echo base_url('auth/index') ?>">Profile</a></li>
                                            <li id="menu-item-38" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-38"><a href="<?php echo base_url('vehicles/index') ?>">All Users Vehicles</a>
                                             <li id="menu-item-43" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-43"><a href="<?php echo base_url('vehicles/register') ?>">Create Vehicle</a></li>
                                             <li id="menu-item-43" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-43"><a href="<?php echo base_url('vehicles/vhllist') ?>">My Vehicles</a></li>
                                            </li>
                                        </ul>            			    </nav>
                                    <div id="responsive-menu-container"></div>
                                </div><!-- .site-navigation -->
                            </div><!--.col-sm-12-->
                            <?php }else{ ?>
                            <div class="col-sm-12">                        
                                <div class="site-navigation">
                                    <nav class="main-menu" style="display: block;">
                                        <ul id="menu-primari_menu" class="header-nav clearfix">
                                        	<li id="menu-item-40" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-40"><a href="<?php echo base_url() ?>">Home</a></li>
                                            <li id="menu-item-40" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-40"><a href="<?php echo base_url('auth/index') ?>">Profile</a></li>
                                           <?php if ($this->session->userdata('ci_session_key_generate') == TRUE) { ?>
                                            <li id="menu-item-38" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-38"><a href="<?php echo base_url('vehicles/vhllist') ?>">My Vehicles</a>
                                                <ul class="sub-menu">
                                                    <li id="menu-item-43" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-43"><a href="<?php echo base_url('vehicles/register') ?>">Create Vehicle</a></li>
                                                </ul>
                                            </li>
                                            <?php } ?>
                                        </ul>            			    </nav>
                                    <div id="responsive-menu-container"></div>
                                </div><!-- .site-navigation -->
                            </div>
                            <?php } ?>
                        </div><!--.row-->
                    </div><!-- .container -->
                </div><!--.header-bottom-inner-->
            </div><!--.header-bottom-->

        </header><!-- #masthead -->

        <div class="main-content">
            <div class="container">
                <div id="content" class="main-content-inner">

                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <div class="row box">
