<div class="row">
  <div class="col-md-12"> <a href="<?php echo base_url('users/register/') ?>" class="btn btn-danger">Create User</a></div>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>ID</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Username</th>
        <th>Edit</th>
        <th>Delete</th>
        <th>Change Password</th>
        <th>New Password</th>
        <th>Status</th>
        <th>All Vehicles Data</th>
      </tr>
    </thead>
    <tbody>
      <?php if($users): ?>
      <?php foreach($users as $user): ?>
      <?php if($user->role == "user"){ ?>
      <tr>
        <td><?php echo $user->id; ?></td>
        <td><?php echo $user->first_name; ?></td>
        <td><?php echo $user->last_name; ?></td>
        <td><?php echo $user->user_name; ?></td>
        <td><a href="<?php echo base_url('users/edit/'.$user->id) ?>" class="btn btn-primary">Edit</a></td>
        <td><a href="javascript:void(0);" class="dltusr btn btn-danger" data-id="<?php print $user->id; ?>">Delete</a></td>
        <td><a href="<?php echo base_url('users/changepwd/'.$user->id) ?>" class="btn btn-primary">Change Password</a></td>
        <td><a href="<?php echo base_url('users/newpwd/'.$user->id) ?>" class="btn btn-primary">New Password</a></td>
        <?php if($user->status == 1){ ?>
        	<td><a href="<?php echo base_url('users/deactivate/'.$user->id) ?>" class="btn btn-success">Activated</a></td>
        <?php }else{ ?>
        	<td><a href="<?php echo base_url('users/activate/'.$user->id) ?>" class="btn btn-danger">Deactivated</a></td>
        <?php } ?>
        <td><a href="<?php echo base_url('vehicles/vhllist/'.$user->id) ?>">View All Vehicles Data</a></td>
      <?php } ?>
      </tr>
      <?php endforeach; ?>
      <?php endif; ?>
    </tbody>
  </table>
</div>
<script type="text/javascript">
jQuery(function($){
   $(".dltusr").click(function(){
	  var getUrl = window.location;
	  var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
      var id = $(this).data("id");
      $.ajax({
        url: baseUrl+'/users/deleteUser/',
		<?php /*?>url: "<?php echo base_url('vehicles/deleteVehicle/'); ?>",<?php */?>
        data: {'id': id},
        type: "POST",
        success: function(status){
			if(status == 'ok'){
                alert('The User has been removed successfully.');
				window.location.reload();
            }else{
                alert('Some problem occurred, please try again.');
            }
        }
      });
   });
});
</script>