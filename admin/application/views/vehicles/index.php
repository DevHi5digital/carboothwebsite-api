<div class="row">
  <div class="col-md-12"> <a href="<?php echo base_url('vehicles/register/') ?>" class="btn btn-danger">Create Vehicle</a></div>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>ID</th>
        <th>UID</th>
        <th>Make</th>
        <th>Model</th>
        <th>Year</th>
        <th>M&M Code</th>
        <th>View User's All Vehicles Data</th>
      </tr>
    </thead>
    <tbody>
      <?php if($vehicles): ?>
      <?php foreach($vehicles as $vehicle): ?>
      <?php if(($vehicle->images_zip_path != "-") || ($vehicle->images_zip_name != "-")): ?>
      <tr>
        <td><?php echo $vehicle->id; ?></td>
        <td><a href="<?php echo base_url('vehicles/vhllist/'.$vehicle->u_id) ?>"><?php echo $vehicle->u_id; ?></a></td>
        <td><?php echo $vehicle->make; ?></td>
        <td><?php echo $vehicle->model; ?></td>
        <td><?php echo $vehicle->year; ?></td>
        <td><?php echo $vehicle->mmcode; ?></td>
        <td><a href="<?php echo base_url('vehicles/vhllist/'.$vehicle->u_id) ?>">View Data</a></td>
      </tr>
      <?php endif; ?>
      <?php endforeach; ?>
      <?php endif; ?>
    </tbody>
  </table>
</div>
