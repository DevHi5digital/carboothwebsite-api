<div class="row page-content">
    <div class="col-lg-12">
        <h2>Vehicle Image</h2>
        <?php if (validation_errors()) { ?>
            <div class="alert alert-danger">
                <?php echo validation_errors(); ?>
            </div>
        <?php } ?>
        <form method="POST" action="<?php echo site_url('vehicles/vimguld');?>" enctype='multipart/form-data'>
        <div class="row">
            <div class="col-lg-12">
            	<p><?php echo $this->session->userdata('vmake'); ?> - <?php echo $this->session->userdata('vmodel'); ?> - <?php echo $this->session->userdata('vyear'); ?></p>
            	<p>Select Any One Background Image</p>
                <div class="form-group">
                    <input type="radio" name="vehiclebgimage" id="vehicle-bgimage" value="1.jpg">
                    <img src="<?php echo base_url('assets/bgimages/1.jpg'); ?>" width="100" height="100"/>
                </div>
                <div class="form-group">
                    <input type="radio" name="vehiclebgimage" id="vehicle-bgimage" value="2.jpg">
                    <img src="<?php echo base_url('assets/bgimages/2.jpg'); ?>" width="100" height="100"/>
                </div>
                <div class="form-group">
                    <input type="radio" name="vehiclebgimage" id="vehicle-bgimage" value="3.jpg">
                    <img src="<?php echo base_url('assets/bgimages/3.jpg'); ?>" width="100" height="100"/>
                </div>
            </div>
        </div>
        <div class="row">	
            <div class="col-lg-12">
                <div class="form-group">
                    <input type="file" name="vehicleimage[]" id="vehicle-image" accept=".png,.jpg,.jpeg,.gif" multiple>
                    <input type="hidden" name="make" value="<?php echo $this->session->userdata('vmake'); ?>">
                    <input type="hidden" name="model" value="<?php echo $this->session->userdata('vmodel'); ?>">
                    <input type="hidden" name="year" value="<?php echo $this->session->userdata('vyear'); ?>">
                    <input type="hidden" name="mmcode" value="<?php echo $this->session->userdata('vmmcode'); ?>">
                </div>
            </div>
        </div>
        <div class="row">	
            <div class="col-lg-12">
                <div class="form-group pull-right">
                    <button type="submit" id="next" class="btn btn-info">Next</button>
                </div>
            </div>
        </div>       
    </div>
    </form>
</div>