<div class="row">
  <?php if($vehicleInfo): ?>
  <?php $vimags = explode(',',$vehicleInfo['vehicle_images']); ?>
  <p><strong>Make: </strong><?php echo $vehicleInfo['make']; ?></p>
  <p><strong>Model: </strong><?php echo $vehicleInfo['model']; ?></p>
  <p><strong>Year: </strong><?php echo $vehicleInfo['year']; ?></p>
  <p><strong>M&M Code: </strong><?php echo $vehicleInfo['mmcode']; ?></p>
  <p><strong>No of Images: </strong><?php echo count($vimags); ?></p>
  <p><strong>Uploaded Date: </strong><?php echo date("j F, Y", strtotime($vehicleInfo['uploaded_date'])); ?></p>
  <p><strong>Download Vehicle Images Zip File: </strong><a href="<?php echo base_url('vehicles/download/'.$vehicleInfo['id']) ?>"><i class="fa fa-file-archive-o" aria-hidden="true" style="color: #c00;font-size: 30px;"></i></a></p>
  <p><strong>Vehicle Images Zip File Send into Mail: </strong><a href="<?php echo base_url('vehicles/sendmail/'.$vehicleInfo['id']) ?>" style="color: #c00;font-weight:bold;">Send Mail</a></p>
  <p><strong>Vehicle Images: </strong><br />
    <?php foreach($vimags as $vimag): ?>
    	<p><img src="<?php echo base_url('assets/vimages/'.$vimag); ?>"/></p>
    <?php endforeach; ?>
  </p>
  <?php endif; ?>
</div>
