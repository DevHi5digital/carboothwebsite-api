<div class="row">
  <div class="col-md-12"> <a href="<?php echo base_url('vehicles/register/') ?>" class="btn btn-danger">Create Vehicle</a></div>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>ID</th>
        <th>Make</th>
        <th>Model</th>
        <th>Year</th>
        <th>M&M Code</th>
        <th>No of Images</th>
        <th>Uploaded Date</th>
        <th>Vehicle Images</th>
        <th>Details View</th>
        <?php /*?><th>Edit</th><?php */?>
        <th>Delete</th>
      </tr>
    </thead>
    <tbody>
      <?php if($vehicles): ?>
      <?php foreach($vehicles as $vehicle): ?>
       <?php if(($vehicle->images_zip_path != "-") || ($vehicle->images_zip_name != "-")): ?>
      <?php $vimags = explode(',',$vehicle->vehicle_images); ?>
      <tr>
        <td><?php echo $vehicle->id; ?></td>
        <td><?php echo $vehicle->make; ?></td>
        <td><?php echo $vehicle->model; ?></td>
        <td><?php echo $vehicle->year; ?></td>
        <td><?php echo $vehicle->mmcode; ?></td>
        <td><?php echo count($vimags); ?></td>
        <td><?php echo date("j F, Y", strtotime($vehicle->uploaded_date));?></td>
        <td>
        	<?php foreach($vimags as $vimag): ?>
        	<img src="<?php echo base_url('assets/vimages/'.$vimag); ?>" width="50" height="50"/>
            <?php endforeach; ?>
        </td>
        <td><a href="<?php echo base_url('vehicles/view/'.$vehicle->id) ?>">View</a></td>
        <?php /*?><td><a href="<?php echo base_url('vehicles/edit/'.$vehicle->id) ?>">Edit</a></td><?php */?>
        <td><a href="javascript:void(0);" class="dltvhl btn btn-danger" data-id="<?php print $vehicle->id; ?>">Delete</a></td>
      </tr>
      <?php endif; ?>
      <?php endforeach; ?>
      <?php endif; ?>
    </tbody>
  </table>
</div>
<script type="text/javascript">
jQuery(function($){
   $(".dltvhl").click(function(){
	  var getUrl = window.location;
	  var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
      var id = $(this).data("id");
      $.ajax({
        url: baseUrl+'/vehicles/deleteVehicle/',
		<?php /*?>url: "<?php echo base_url('vehicles/deleteVehicle/'); ?>",<?php */?>
        data: {'id': id},
        type: "POST",
        success: function(status){
			if(status == 'ok'){
                alert('The Vehicle has been removed successfully.');
				window.location.reload();
            }else{
                alert('Some problem occurred, please try again.');
            }
        }
      });
   });
});
</script>