<div class="row page-content">
    <div class="col-lg-12">
        <h2>Vehicle Image Process</h2>
        <?php if (validation_errors()) { ?>
            <div class="alert alert-danger">
                <?php echo validation_errors(); ?>
            </div>
        <?php } ?>
        <form method="POST" action="<?php echo site_url('vehicles/vimguldpc');?>" enctype='multipart/form-data'>
        <?php $deliveryData   = $this->session->userdata('vehicleInfo'); ?>
        <div class="row">	
            <div class="col-lg-12">
                <div class="form-group">
                	<p><?php echo $this->session->userdata('vmake'); ?> - <?php echo $this->session->userdata('vmodel'); ?> - <?php echo $this->session->userdata('vyear'); ?></p>
                    <input type="hidden" name="id" value="<?php echo $this->session->userdata['vehicleInfo']['id']; ?>">
                    <input type="hidden" name="make" value="<?php echo $this->session->userdata('vmake'); ?>">
                    <input type="hidden" name="model" value="<?php echo $this->session->userdata('vmodel'); ?>">
                    <input type="hidden" name="year" value="<?php echo $this->session->userdata('vyear'); ?>">
                    <input type="hidden" name="mmcode" value="<?php echo $this->session->userdata('vmmcode'); ?>">
                </div>
            </div>
        </div>
        <div class="row">	
            <div class="col-lg-12">
                <div class="form-group">
                    <?php $bf_vimgs = explode(',',$this->session->userdata['vehicleInfo']['vehicle_images']); $vimgs = array_filter($bf_vimgs); ?>
                    <?php if(!empty($vimgs)){ ?>
                        <div class="vehicle-imgs">
                        <?php foreach($vimgs as $vimg){ ?>
                            <div class="col-md-2">
                                <div><img src="<?php echo base_url('assets/vimages/'.$vimg); ?>" width="200" height="200"></div>
                                 <p><a href="javascript:void(0);" class="dltvimg btn btn-default" data-id="<?php print $this->session->userdata['vehicleInfo']['id']; ?>" data-title="<?php print $vimg; ?>">Delete</a></p>
                            </div>
                        <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="row">	
            <div class="col-lg-12">
                <div class="form-group pull-right">
                    <button type="submit" id="process" class="btn btn-info">Process Images Backgrounds</button>
                </div>
            </div>
        </div>       
    </div>
    </form>
</div>
<script type="text/javascript">
jQuery(function($){
   $(".dltvimg").click(function(){
	  var getUrl = window.location;
	  var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
      var id = $(this).data("id");
	  var title = $(this).data("title");
      $.ajax({
        url: baseUrl+'/vehicles/deleteVehicleImg/',
		<?php /*?>url: "<?php echo base_url('vehicles/deleteVehicleImg/'); ?>",<?php */?>
        data: {'id': id,'title': title},
        type: "POST",
        success: function(status){
			if(status == 'ok'){
                alert('The Image has been removed successfully.');
				window.location.reload();
            }else{
                alert('Some problem occurred, please try again.');
            }
        }
      });
   });
});
</script>