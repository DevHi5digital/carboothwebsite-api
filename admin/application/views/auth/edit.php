<div class="row page-content">
    <div class="col-lg-12">
        <h2>Profile</h2>
        <div class="form-group text-right">
            <a class="btn btn-primary btn-xs" href="<?php print site_url() ?>profile"><i class="fa fa-user"></i> Profile</a>
            <a class="btn btn-info btn-xs" href="<?php print site_url() ?>profile/edit"><i class="fa fa-edit"></i> Edit</a>
            <a class="btn btn-warning btn-xs" href="<?php print site_url() ?>setting"><i class="fa fa-gear"></i> Settings</a>
            <a class="btn btn-danger btn-xs" href="<?php print site_url() ?>auth/logout"><i class="fa fa-power-off"></i> Log Out</a>
        </div>
        <?php if (validation_errors()) { ?>
            <div class="alert alert-danger">
                <?php echo validation_errors(); ?>
            </div>
        <?php } ?>        
        <?php echo form_open('auth/editUser'); ?>        
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-user-o"></i>
                        </span>
                        <input type="text" name="first_name" class="form-control" id="first-name" placeholder="First Name" value="<?php print $userInfo['first_name']; ?>">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-user-o"></i>
                        </span>
                        <input type="text" name="last_name" class="form-control" id="last-name" placeholder="Last Name (Surname)" value="<?php print $userInfo['last_name']; ?>">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">            			
            <div class="col-lg-6">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-envelope"></i>
                        </span>
                        <input type="text" disabled="disabled" name="email" class="form-control" id="email" placeholder="Email" value="<?php print $userInfo['email']; ?>">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-phone"></i>
                        </span>
                        <input type="text" name="contact_no" class="form-control" id="contact-no" placeholder="Contact No" value="<?php print $userInfo['contact_no']; ?>">
                    </div>
                </div>
            </div>
        </div>        
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-map-marker"></i>
                        </span>
                        <textarea type="text" name="address" class="form-control" id="address" placeholder="Address"><?php print $userInfo['address']; ?></textarea>
                    </div>
                </div>
            </div>			
            <div class='col-lg-6'>
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </span>
                        <input type='text' class="form-control" name="company_name" id="company-name" placeholder="Company Name" value="<?php print $userInfo['company_name']; ?>">                    
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-map-marker"></i>
                        </span>
                        <select id="country" name="country" class="form-control">
                        	<option value="">Select Country</option>
                            <option value="Afghanistan" <?php if($userInfo['country'] == "Afghanistan"){ echo "selected='selected'"; } ?>>Afghanistan</option>
                            <option value="Åland Islands" <?php if($userInfo['country'] == "Åland Islands"){ echo "selected='selected'"; } ?>>Åland Islands</option>
                            <option value="Albania" <?php if($userInfo['country'] == "Albania"){ echo "selected='selected'"; } ?>>Albania</option>
                            <option value="Algeria" <?php if($userInfo['country'] == "Algeria"){ echo "selected='selected'"; } ?>>Algeria</option>
                            <option value="American Samoa" <?php if($userInfo['country'] == "American Samoa"){ echo "selected='selected'"; } ?>>American Samoa</option>
                            <option value="Andorra" <?php if($userInfo['country'] == "Andorra"){ echo "selected='selected'"; } ?>>Andorra</option>
                            <option value="Angola" <?php if($userInfo['country'] == "Angola"){ echo "selected='selected'"; } ?>>Angola</option>
                            <option value="Anguilla" <?php if($userInfo['country'] == "Anguilla"){ echo "selected='selected'"; } ?>>Anguilla</option>
                            <option value="Antarctica" <?php if($userInfo['country'] == "Antarctica"){ echo "selected='selected'"; } ?>>Antarctica</option>
                            <option value="Antigua and Barbuda" <?php if($userInfo['country'] == "Antigua and Barbuda"){ echo "selected='selected'"; } ?>>Antigua and Barbuda</option>
                            <option value="Argentina" <?php if($userInfo['country'] == "Argentina"){ echo "selected='selected'"; } ?>>Argentina</option>
                            <option value="Armenia" <?php if($userInfo['country'] == "Armenia"){ echo "selected='selected'"; } ?>>Armenia</option>
                            <option value="Aruba" <?php if($userInfo['country'] == "Aruba"){ echo "selected='selected'"; } ?>>Aruba</option>
                            <option value="Australia" <?php if($userInfo['country'] == "Australia"){ echo "selected='selected'"; } ?>>Australia</option>
                            <option value="Austria" <?php if($userInfo['country'] == "Austria"){ echo "selected='selected'"; } ?>>Austria</option>
                            <option value="Azerbaijan" <?php if($userInfo['country'] == "Azerbaijan"){ echo "selected='selected'"; } ?>>Azerbaijan</option>
                            <option value="Bahamas" <?php if($userInfo['country'] == "Bahamas"){ echo "selected='selected'"; } ?>>Bahamas</option>
                            <option value="Bahrain" <?php if($userInfo['country'] == "Bahrain"){ echo "selected='selected'"; } ?>>Bahrain</option>
                            <option value="Bangladesh" <?php if($userInfo['country'] == "Bangladesh"){ echo "selected='selected'"; } ?>>Bangladesh</option>
                            <option value="Barbados" <?php if($userInfo['country'] == "Barbados"){ echo "selected='selected'"; } ?>>Barbados</option>
                            <option value="Belarus" <?php if($userInfo['country'] == "Belarus"){ echo "selected='selected'"; } ?>>Belarus</option>
                            <option value="Belgium" <?php if($userInfo['country'] == "Belgium"){ echo "selected='selected'"; } ?>>Belgium</option>
                            <option value="Belize" <?php if($userInfo['country'] == "Belize"){ echo "selected='selected'"; } ?>>Belize</option>
                            <option value="Benin" <?php if($userInfo['country'] == "Benin"){ echo "selected='selected'"; } ?>>Benin</option>
                            <option value="Bermuda" <?php if($userInfo['country'] == "Bermuda"){ echo "selected='selected'"; } ?>>Bermuda</option>
                            <option value="Bhutan" <?php if($userInfo['country'] == "Bhutan"){ echo "selected='selected'"; } ?>>Bhutan</option>
                            <option value="Bolivia" <?php if($userInfo['country'] == "Bolivia"){ echo "selected='selected'"; } ?>>Bolivia</option>
                            <option value="Bosnia and Herzegovina" <?php if($userInfo['country'] == "Bosnia and Herzegovina"){ echo "selected='selected'"; } ?>>Bosnia and Herzegovina</option>
                            <option value="Botswana" <?php if($userInfo['country'] == "Botswana"){ echo "selected='selected'"; } ?>>Botswana</option>
                            <option value="Bouvet Island" <?php if($userInfo['country'] == "Bouvet Island"){ echo "selected='selected'"; } ?>>Bouvet Island</option>
                            <option value="Brazil" <?php if($userInfo['country'] == "Brazil"){ echo "selected='selected'"; } ?>>Brazil</option>
                            <option value="British Indian Ocean Territory" <?php if($userInfo['country'] == "British Indian Ocean Territory"){ echo "selected='selected'"; } ?>>British Indian Ocean Territory</option>
                            <option value="Brunei Darussalam" <?php if($userInfo['country'] == "Brunei Darussalam"){ echo "selected='selected'"; } ?>>Brunei Darussalam</option>
                            <option value="Bulgaria" <?php if($userInfo['country'] == "Bulgaria"){ echo "selected='selected'"; } ?>>Bulgaria</option>
                            <option value="Burkina Faso" <?php if($userInfo['country'] == "Burkina Faso"){ echo "selected='selected'"; } ?>>Burkina Faso</option>
                            <option value="Burundi" <?php if($userInfo['country'] == "Burundi"){ echo "selected='selected'"; } ?>>Burundi</option>
                            <option value="Cambodia" <?php if($userInfo['country'] == "Cambodia"){ echo "selected='selected'"; } ?>>Cambodia</option>
                            <option value="Cameroon" <?php if($userInfo['country'] == "Cameroon"){ echo "selected='selected'"; } ?>>Cameroon</option>
                            <option value="Canada" <?php if($userInfo['country'] == "Canada"){ echo "selected='selected'"; } ?>>Canada</option>
                            <option value="Cape Verde" <?php if($userInfo['country'] == "Cape Verde"){ echo "selected='selected'"; } ?>>Cape Verde</option>
                            <option value="Cayman Islands" <?php if($userInfo['country'] == "Cayman Islands"){ echo "selected='selected'"; } ?>>Cayman Islands</option>
                            <option value="Central African Republic" <?php if($userInfo['country'] == "Central African Republic"){ echo "selected='selected'"; } ?>>Central African Republic</option>
                            <option value="Chad" <?php if($userInfo['country'] == "Chad"){ echo "selected='selected'"; } ?>>Chad</option>
                            <option value="Chile" <?php if($userInfo['country'] == "Chile"){ echo "selected='selected'"; } ?>>Chile</option>
                            <option value="China" <?php if($userInfo['country'] == "China"){ echo "selected='selected'"; } ?>>China</option>
                            <option value="Christmas Island" <?php if($userInfo['country'] == "Christmas Island"){ echo "selected='selected'"; } ?>>Christmas Island</option>
                            <option value="Cocos (Keeling) Islands" <?php if($userInfo['country'] == "Cocos (Keeling) Islands"){ echo "selected='selected'"; } ?>>Cocos (Keeling) Islands</option>
                            <option value="Colombia" <?php if($userInfo['country'] == "Colombia"){ echo "selected='selected'"; } ?>>Colombia</option>
                            <option value="Comoros" <?php if($userInfo['country'] == "Comoros"){ echo "selected='selected'"; } ?>>Comoros</option>
                            <option value="Congo" <?php if($userInfo['country'] == "Congo"){ echo "selected='selected'"; } ?>>Congo</option>
                            <option value="Congo, The Democratic Republic of The" <?php if($userInfo['country'] == "Congo, The Democratic Republic of The"){ echo "selected='selected'"; } ?>>Congo, The Democratic Republic of The</option>
                            <option value="Cook Islands" <?php if($userInfo['country'] == "Cook Islands"){ echo "selected='selected'"; } ?>>Cook Islands</option>
                            <option value="Costa Rica" <?php if($userInfo['country'] == "Costa Rica"){ echo "selected='selected'"; } ?>>Costa Rica</option>
                            <option value="Cote D'ivoire" <?php if($userInfo['country'] == "Cote D'ivoire"){ echo "selected='selected'"; } ?>>Cote D'ivoire</option>
                            <option value="Croatia" <?php if($userInfo['country'] == "Croatia"){ echo "selected='selected'"; } ?>>Croatia</option>
                            <option value="Cuba" <?php if($userInfo['country'] == "Cuba"){ echo "selected='selected'"; } ?>>Cuba</option>
                            <option value="Cyprus" <?php if($userInfo['country'] == "Cyprus"){ echo "selected='selected'"; } ?>>Cyprus</option>
                            <option value="Czech Republic" <?php if($userInfo['country'] == "Czech Republic"){ echo "selected='selected'"; } ?>>Czech Republic</option>
                            <option value="Denmark" <?php if($userInfo['country'] == "Denmark"){ echo "selected='selected'"; } ?>>Denmark</option>
                            <option value="Djibouti" <?php if($userInfo['country'] == "Djibouti"){ echo "selected='selected'"; } ?>>Djibouti</option>
                            <option value="Dominica" <?php if($userInfo['country'] == "Dominica"){ echo "selected='selected'"; } ?>>Dominica</option>
                            <option value="Dominican Republic" <?php if($userInfo['country'] == "Dominican Republic"){ echo "selected='selected'"; } ?>>Dominican Republic</option>
                            <option value="Ecuador" <?php if($userInfo['country'] == "Ecuador"){ echo "selected='selected'"; } ?>>Ecuador</option>
                            <option value="Egypt" <?php if($userInfo['country'] == "Egypt"){ echo "selected='selected'"; } ?>>Egypt</option>
                            <option value="El Salvador" <?php if($userInfo['country'] == "El Salvador"){ echo "selected='selected'"; } ?>>El Salvador</option>
                            <option value="Equatorial Guinea" <?php if($userInfo['country'] == "Equatorial Guinea"){ echo "selected='selected'"; } ?>>Equatorial Guinea</option>
                            <option value="Eritrea" <?php if($userInfo['country'] == "Eritrea"){ echo "selected='selected'"; } ?>>Eritrea</option>
                            <option value="Estonia" <?php if($userInfo['country'] == "Estonia"){ echo "selected='selected'"; } ?>>Estonia</option>
                            <option value="Ethiopia" <?php if($userInfo['country'] == "Ethiopia"){ echo "selected='selected'"; } ?>>Ethiopia</option>
                            <option value="Falkland Islands (Malvinas)" <?php if($userInfo['country'] == "Falkland Islands (Malvinas)"){ echo "selected='selected'"; } ?>>Falkland Islands (Malvinas)</option>
                            <option value="Faroe Islands" <?php if($userInfo['country'] == "Faroe Islands"){ echo "selected='selected'"; } ?>>Faroe Islands</option>
                            <option value="Fiji" <?php if($userInfo['country'] == "Fiji"){ echo "selected='selected'"; } ?>>Fiji</option>
                            <option value="Finland" <?php if($userInfo['country'] == "Finland"){ echo "selected='selected'"; } ?>>Finland</option>
                            <option value="France" <?php if($userInfo['country'] == "France"){ echo "selected='selected'"; } ?>>France</option>
                            <option value="French Guiana" <?php if($userInfo['country'] == "French Guiana"){ echo "selected='selected'"; } ?>>French Guiana</option>
                            <option value="French Polynesia" <?php if($userInfo['country'] == "French Polynesia"){ echo "selected='selected'"; } ?>>French Polynesia</option>
                            <option value="French Southern Territories" <?php if($userInfo['country'] == "French Southern Territories"){ echo "selected='selected'"; } ?>>French Southern Territories</option>
                            <option value="Gabon" <?php if($userInfo['country'] == "Gabon"){ echo "selected='selected'"; } ?>>Gabon</option>
                            <option value="Gambia" <?php if($userInfo['country'] == "Gambia"){ echo "selected='selected'"; } ?>>Gambia</option>
                            <option value="Georgia" <?php if($userInfo['country'] == "Georgia"){ echo "selected='selected'"; } ?>>Georgia</option>
                            <option value="Germany" <?php if($userInfo['country'] == "Germany"){ echo "selected='selected'"; } ?>>Germany</option>
                            <option value="Ghana" <?php if($userInfo['country'] == "Ghana"){ echo "selected='selected'"; } ?>>Ghana</option>
                            <option value="Gibraltar" <?php if($userInfo['country'] == "Gibraltar"){ echo "selected='selected'"; } ?>>Gibraltar</option>
                            <option value="Greece" <?php if($userInfo['country'] == "Greece"){ echo "selected='selected'"; } ?>>Greece</option>
                            <option value="Greenland" <?php if($userInfo['country'] == "Greenland"){ echo "selected='selected'"; } ?>>Greenland</option>
                            <option value="Grenada" <?php if($userInfo['country'] == "Grenada"){ echo "selected='selected'"; } ?>>Grenada</option>
                            <option value="Guadeloupe" <?php if($userInfo['country'] == "Guadeloupe"){ echo "selected='selected'"; } ?>>Guadeloupe</option>
                            <option value="Guam" <?php if($userInfo['country'] == "Guam"){ echo "selected='selected'"; } ?>>Guam</option>
                            <option value="Guatemala" <?php if($userInfo['country'] == "Guatemala"){ echo "selected='selected'"; } ?>>Guatemala</option>
                            <option value="Guernsey" <?php if($userInfo['country'] == "Guernsey"){ echo "selected='selected'"; } ?>>Guernsey</option>
                            <option value="Guinea" <?php if($userInfo['country'] == "Guinea"){ echo "selected='selected'"; } ?>>Guinea</option>
                            <option value="Guinea-bissau" <?php if($userInfo['country'] == "Guinea-bissau"){ echo "selected='selected'"; } ?>>Guinea-bissau</option>
                            <option value="Guyana" <?php if($userInfo['country'] == "Guyana"){ echo "selected='selected'"; } ?>>Guyana</option>
                            <option value="Haiti" <?php if($userInfo['country'] == "Haiti"){ echo "selected='selected'"; } ?>>Haiti</option>
                            <option value="Heard Island and Mcdonald Islands" <?php if($userInfo['country'] == "Heard Island and Mcdonald Islands"){ echo "selected='selected'"; } ?>>Heard Island and Mcdonald Islands</option>
                            <option value="Holy See (Vatican City State)" <?php if($userInfo['country'] == "Holy See (Vatican City State)"){ echo "selected='selected'"; } ?>>Holy See (Vatican City State)</option>
                            <option value="Honduras" <?php if($userInfo['country'] == "Honduras"){ echo "selected='selected'"; } ?>>Honduras</option>
                            <option value="Hong Kong" <?php if($userInfo['country'] == "Hong Kong"){ echo "selected='selected'"; } ?>>Hong Kong</option>
                            <option value="Hungary" <?php if($userInfo['country'] == "Hungary"){ echo "selected='selected'"; } ?>>Hungary</option>
                            <option value="Iceland" <?php if($userInfo['country'] == "Iceland"){ echo "selected='selected'"; } ?>>Iceland</option>
                            <option value="India" <?php if($userInfo['country'] == "India"){ echo "selected='selected'"; } ?>>India</option>
                            <option value="Indonesia" <?php if($userInfo['country'] == "Indonesia"){ echo "selected='selected'"; } ?>>Indonesia</option>
                            <option value="Iran, Islamic Republic of" <?php if($userInfo['country'] == "Iran, Islamic Republic of"){ echo "selected='selected'"; } ?>>Iran, Islamic Republic of</option>
                            <option value="Iraq" <?php if($userInfo['country'] == "Iraq"){ echo "selected='selected'"; } ?>>Iraq</option>
                            <option value="Ireland" <?php if($userInfo['country'] == "Ireland"){ echo "selected='selected'"; } ?>>Ireland</option>
                            <option value="Isle of Man" <?php if($userInfo['country'] == "Isle of Man"){ echo "selected='selected'"; } ?>>Isle of Man</option>
                            <option value="Israel" <?php if($userInfo['country'] == "Israel"){ echo "selected='selected'"; } ?>>Israel</option>
                            <option value="Italy" <?php if($userInfo['country'] == "Italy"){ echo "selected='selected'"; } ?>>Italy</option>
                            <option value="Jamaica" <?php if($userInfo['country'] == "Jamaica"){ echo "selected='selected'"; } ?>>Jamaica</option>
                            <option value="Japan" <?php if($userInfo['country'] == "Japan"){ echo "selected='selected'"; } ?>>Japan</option>
                            <option value="Jersey" <?php if($userInfo['country'] == "Jersey"){ echo "selected='selected'"; } ?>>Jersey</option>
                            <option value="Jordan" <?php if($userInfo['country'] == "Jordan"){ echo "selected='selected'"; } ?>>Jordan</option>
                            <option value="Kazakhstan" <?php if($userInfo['country'] == "Kazakhstan"){ echo "selected='selected'"; } ?>>Kazakhstan</option>
                            <option value="Kenya" <?php if($userInfo['country'] == "Kenya"){ echo "selected='selected'"; } ?>>Kenya</option>
                            <option value="Kiribati" <?php if($userInfo['country'] == "Kiribati"){ echo "selected='selected'"; } ?>>Kiribati</option>
                            <option value="Korea, Democratic People's Republic of" <?php if($userInfo['country'] == "Korea, Democratic People's Republic of"){ echo "selected='selected'"; } ?>>Korea, Democratic People's Republic of</option>
                            <option value="Korea, Republic of" <?php if($userInfo['country'] == "Korea, Republic of"){ echo "selected='selected'"; } ?>>Korea, Republic of</option>
                            <option value="Kuwait" <?php if($userInfo['country'] == "Kuwait"){ echo "selected='selected'"; } ?>>Kuwait</option>
                            <option value="Kyrgyzstan" <?php if($userInfo['country'] == "Kyrgyzstan"){ echo "selected='selected'"; } ?>>Kyrgyzstan</option>
                            <option value="Lao People's Democratic Republic" <?php if($userInfo['country'] == "Lao People's Democratic Republic"){ echo "selected='selected'"; } ?>>Lao People's Democratic Republic</option>
                            <option value="Latvia" <?php if($userInfo['country'] == "Latvia"){ echo "selected='selected'"; } ?>>Latvia</option>
                            <option value="Lebanon" <?php if($userInfo['country'] == "Lebanon"){ echo "selected='selected'"; } ?>>Lebanon</option>
                            <option value="Lesotho" <?php if($userInfo['country'] == "Lesotho"){ echo "selected='selected'"; } ?>>Lesotho</option>
                            <option value="Liberia" <?php if($userInfo['country'] == "Liberia"){ echo "selected='selected'"; } ?>>Liberia</option>
                            <option value="Libyan Arab Jamahiriya" <?php if($userInfo['country'] == "Libyan Arab Jamahiriya"){ echo "selected='selected'"; } ?>>Libyan Arab Jamahiriya</option>
                            <option value="Liechtenstein" <?php if($userInfo['country'] == "Liechtenstein"){ echo "selected='selected'"; } ?>>Liechtenstein</option>
                            <option value="Lithuania" <?php if($userInfo['country'] == "Lithuania"){ echo "selected='selected'"; } ?>>Lithuania</option>
                            <option value="Luxembourg" <?php if($userInfo['country'] == "Luxembourg"){ echo "selected='selected'"; } ?>>Luxembourg</option>
                            <option value="Macao" <?php if($userInfo['country'] == "Macao"){ echo "selected='selected'"; } ?>>Macao</option>
                            <option value="Macedonia, The Former Yugoslav Republic of" <?php if($userInfo['country'] == "Macedonia, The Former Yugoslav Republic of"){ echo "selected='selected'"; } ?>>Macedonia, The Former Yugoslav Republic of</option>
                            <option value="Madagascar" <?php if($userInfo['country'] == "Madagascar"){ echo "selected='selected'"; } ?>>Madagascar</option>
                            <option value="Malawi" <?php if($userInfo['country'] == "Malawi"){ echo "selected='selected'"; } ?>>Malawi</option>
                            <option value="Malaysia" <?php if($userInfo['country'] == "Malaysia"){ echo "selected='selected'"; } ?>>Malaysia</option>
                            <option value="Maldives" <?php if($userInfo['country'] == "Maldives"){ echo "selected='selected'"; } ?>>Maldives</option>
                            <option value="Mali" <?php if($userInfo['country'] == "Mali"){ echo "selected='selected'"; } ?>>Mali</option>
                            <option value="Malta" <?php if($userInfo['country'] == "Malta"){ echo "selected='selected'"; } ?>>Malta</option>
                            <option value="Marshall Islands" <?php if($userInfo['country'] == "Marshall Islands"){ echo "selected='selected'"; } ?>>Marshall Islands</option>
                            <option value="Martinique" <?php if($userInfo['country'] == "Martinique"){ echo "selected='selected'"; } ?>>Martinique</option>
                            <option value="Mauritania" <?php if($userInfo['country'] == "Mauritania"){ echo "selected='selected'"; } ?>>Mauritania</option>
                            <option value="Mauritius" <?php if($userInfo['country'] == "Mauritius"){ echo "selected='selected'"; } ?>>Mauritius</option>
                            <option value="Mayotte" <?php if($userInfo['country'] == "Mayotte"){ echo "selected='selected'"; } ?>>Mayotte</option>
                            <option value="Mexico" <?php if($userInfo['country'] == "Mexico"){ echo "selected='selected'"; } ?>>Mexico</option>
                            <option value="Micronesia, Federated States of" <?php if($userInfo['country'] == "Micronesia, Federated States of"){ echo "selected='selected'"; } ?>>Micronesia, Federated States of</option>
                            <option value="Moldova, Republic of" <?php if($userInfo['country'] == "Moldova, Republic of"){ echo "selected='selected'"; } ?>>Moldova, Republic of</option>
                            <option value="Monaco" <?php if($userInfo['country'] == "Monaco"){ echo "selected='selected'"; } ?>>Monaco</option>
                            <option value="Mongolia" <?php if($userInfo['country'] == "Mongolia"){ echo "selected='selected'"; } ?>>Mongolia</option>
                            <option value="Montenegro" <?php if($userInfo['country'] == "Montenegro"){ echo "selected='selected'"; } ?>>Montenegro</option>
                            <option value="Montserrat" <?php if($userInfo['country'] == "Montserrat"){ echo "selected='selected'"; } ?>>Montserrat</option>
                            <option value="Morocco" <?php if($userInfo['country'] == "Morocco"){ echo "selected='selected'"; } ?>>Morocco</option>
                            <option value="Mozambique" <?php if($userInfo['country'] == "Mozambique"){ echo "selected='selected'"; } ?>>Mozambique</option>
                            <option value="Myanmar" <?php if($userInfo['country'] == "Myanmar"){ echo "selected='selected'"; } ?>>Myanmar</option>
                            <option value="Namibia" <?php if($userInfo['country'] == "Namibia"){ echo "selected='selected'"; } ?>>Namibia</option>
                            <option value="Nauru" <?php if($userInfo['country'] == "Nauru"){ echo "selected='selected'"; } ?>>Nauru</option>
                            <option value="Nepal" <?php if($userInfo['country'] == "Nepal"){ echo "selected='selected'"; } ?>>Nepal</option>
                            <option value="Netherlands" <?php if($userInfo['country'] == "Netherlands"){ echo "selected='selected'"; } ?>>Netherlands</option>
                            <option value="Netherlands Antilles" <?php if($userInfo['country'] == "Netherlands Antilles"){ echo "selected='selected'"; } ?>>Netherlands Antilles</option>
                            <option value="New Caledonia" <?php if($userInfo['country'] == "New Caledonia"){ echo "selected='selected'"; } ?>>New Caledonia</option>
                            <option value="New Zealand" <?php if($userInfo['country'] == "New Zealand"){ echo "selected='selected'"; } ?>>New Zealand</option>
                            <option value="Nicaragua" <?php if($userInfo['country'] == "Nicaragua"){ echo "selected='selected'"; } ?>>Nicaragua</option>
                            <option value="Niger" <?php if($userInfo['country'] == "Niger"){ echo "selected='selected'"; } ?>>Niger</option>
                            <option value="Nigeria" <?php if($userInfo['country'] == "Nigeria"){ echo "selected='selected'"; } ?>>Nigeria</option>
                            <option value="Niue" <?php if($userInfo['country'] == "Niue"){ echo "selected='selected'"; } ?>>Niue</option>
                            <option value="Norfolk Island" <?php if($userInfo['country'] == "Norfolk Island"){ echo "selected='selected'"; } ?>>Norfolk Island</option>
                            <option value="Northern Mariana Islands" <?php if($userInfo['country'] == "Northern Mariana Islands"){ echo "selected='selected'"; } ?>>Northern Mariana Islands</option>
                            <option value="Norway" <?php if($userInfo['country'] == "Norway"){ echo "selected='selected'"; } ?>>Norway</option>
                            <option value="Oman" <?php if($userInfo['country'] == "Oman"){ echo "selected='selected'"; } ?>>Oman</option>
                            <option value="Pakistan" <?php if($userInfo['country'] == "Pakistan"){ echo "selected='selected'"; } ?>>Pakistan</option>
                            <option value="Palau" <?php if($userInfo['country'] == "Palau"){ echo "selected='selected'"; } ?>>Palau</option>
                            <option value="Palestinian Territory, Occupied" <?php if($userInfo['country'] == "Palestinian Territory, Occupied"){ echo "selected='selected'"; } ?>>Palestinian Territory, Occupied</option>
                            <option value="Panama" <?php if($userInfo['country'] == "Panama"){ echo "selected='selected'"; } ?>>Panama</option>
                            <option value="Papua New Guinea" <?php if($userInfo['country'] == "Papua New Guinea"){ echo "selected='selected'"; } ?>>Papua New Guinea</option>
                            <option value="Paraguay" <?php if($userInfo['country'] == "Paraguay"){ echo "selected='selected'"; } ?>>Paraguay</option>
                            <option value="Peru" <?php if($userInfo['country'] == "Peru"){ echo "selected='selected'"; } ?>>Peru</option>
                            <option value="Philippines" <?php if($userInfo['country'] == "Philippines"){ echo "selected='selected'"; } ?>>Philippines</option>
                            <option value="Pitcairn" <?php if($userInfo['country'] == "Pitcairn"){ echo "selected='selected'"; } ?>>Pitcairn</option>
                            <option value="Poland" <?php if($userInfo['country'] == "Poland"){ echo "selected='selected'"; } ?>>Poland</option>
                            <option value="Portugal" <?php if($userInfo['country'] == "Portugal"){ echo "selected='selected'"; } ?>>Portugal</option>
                            <option value="Puerto Rico" <?php if($userInfo['country'] == "Puerto Rico"){ echo "selected='selected'"; } ?>>Puerto Rico</option>
                            <option value="Qatar" <?php if($userInfo['country'] == "Qatar"){ echo "selected='selected'"; } ?>>Qatar</option>
                            <option value="Reunion" <?php if($userInfo['country'] == "Reunion"){ echo "selected='selected'"; } ?>>Reunion</option>
                            <option value="Romania" <?php if($userInfo['country'] == "Romania"){ echo "selected='selected'"; } ?>>Romania</option>
                            <option value="Russian Federation" <?php if($userInfo['country'] == "Russian Federation"){ echo "selected='selected'"; } ?>>Russian Federation</option>
                            <option value="Rwanda" <?php if($userInfo['country'] == "Rwanda"){ echo "selected='selected'"; } ?>>Rwanda</option>
                            <option value="Saint Helena" <?php if($userInfo['country'] == "Saint Helena"){ echo "selected='selected'"; } ?>>Saint Helena</option>
                            <option value="Saint Kitts and Nevis" <?php if($userInfo['country'] == "Saint Kitts and Nevis"){ echo "selected='selected'"; } ?>>Saint Kitts and Nevis</option>
                            <option value="Saint Lucia" <?php if($userInfo['country'] == "Saint Lucia"){ echo "selected='selected'"; } ?>>Saint Lucia</option>
                            <option value="Saint Pierre and Miquelon" <?php if($userInfo['country'] == "Saint Pierre and Miquelon"){ echo "selected='selected'"; } ?>>Saint Pierre and Miquelon</option>
                            <option value="Saint Vincent and The Grenadines" <?php if($userInfo['country'] == "Saint Vincent and The Grenadines"){ echo "selected='selected'"; } ?>>Saint Vincent and The Grenadines</option>
                            <option value="Samoa" <?php if($userInfo['country'] == "Samoa"){ echo "selected='selected'"; } ?>>Samoa</option>
                            <option value="San Marino" <?php if($userInfo['country'] == "San Marino"){ echo "selected='selected'"; } ?>>San Marino</option>
                            <option value="Sao Tome and Principe" <?php if($userInfo['country'] == "Sao Tome and Principe"){ echo "selected='selected'"; } ?>>Sao Tome and Principe</option>
                            <option value="Saudi Arabia" <?php if($userInfo['country'] == "Saudi Arabia"){ echo "selected='selected'"; } ?>>Saudi Arabia</option>
                            <option value="Senegal" <?php if($userInfo['country'] == "Senegal"){ echo "selected='selected'"; } ?>>Senegal</option>
                            <option value="Serbia" <?php if($userInfo['country'] == "Serbia"){ echo "selected='selected'"; } ?>>Serbia</option>
                            <option value="Seychelles" <?php if($userInfo['country'] == "Seychelles"){ echo "selected='selected'"; } ?>>Seychelles</option>
                            <option value="Sierra Leone" <?php if($userInfo['country'] == "Sierra Leone"){ echo "selected='selected'"; } ?>>Sierra Leone</option>
                            <option value="Singapore" <?php if($userInfo['country'] == "Singapore"){ echo "selected='selected'"; } ?>>Singapore</option>
                            <option value="Slovakia" <?php if($userInfo['country'] == "Slovakia"){ echo "selected='selected'"; } ?>>Slovakia</option>
                            <option value="Slovenia" <?php if($userInfo['country'] == "Slovenia"){ echo "selected='selected'"; } ?>>Slovenia</option>
                            <option value="Solomon Islands" <?php if($userInfo['country'] == "Solomon Islands"){ echo "selected='selected'"; } ?>>Solomon Islands</option>
                            <option value="Somalia" <?php if($userInfo['country'] == "Somalia"){ echo "selected='selected'"; } ?>>Somalia</option>
                            <option value="South Africa" <?php if($userInfo['country'] == "South Africa"){ echo "selected='selected'"; } ?>>South Africa</option>
                            <option value="South Georgia and The South Sandwich Islands" <?php if($userInfo['country'] == "South Georgia and The South Sandwich Islands"){ echo "selected='selected'"; } ?>>South Georgia and The South Sandwich Islands</option>
                            <option value="Spain" <?php if($userInfo['country'] == "Spain"){ echo "selected='selected'"; } ?>>Spain</option>
                            <option value="Sri Lanka" <?php if($userInfo['country'] == "Sri Lanka"){ echo "selected='selected'"; } ?>>Sri Lanka</option>
                            <option value="Sudan" <?php if($userInfo['country'] == "Sudan"){ echo "selected='selected'"; } ?>>Sudan</option>
                            <option value="Suriname" <?php if($userInfo['country'] == "Suriname"){ echo "selected='selected'"; } ?>>Suriname</option>
                            <option value="Svalbard and Jan Mayen" <?php if($userInfo['country'] == "Svalbard and Jan Mayen"){ echo "selected='selected'"; } ?>>Svalbard and Jan Mayen</option>
                            <option value="Swaziland" <?php if($userInfo['country'] == "Swaziland"){ echo "selected='selected'"; } ?>>Swaziland</option>
                            <option value="Sweden" <?php if($userInfo['country'] == "Sweden"){ echo "selected='selected'"; } ?>>Sweden</option>
                            <option value="Switzerland" <?php if($userInfo['country'] == "Switzerland"){ echo "selected='selected'"; } ?>>Switzerland</option>
                            <option value="Syrian Arab Republic" <?php if($userInfo['country'] == "Syrian Arab Republic"){ echo "selected='selected'"; } ?>>Syrian Arab Republic</option>
                            <option value="Taiwan, Province of China" <?php if($userInfo['country'] == "Taiwan, Province of China"){ echo "selected='selected'"; } ?>>Taiwan, Province of China</option>
                            <option value="Tajikistan" <?php if($userInfo['country'] == "Tajikistan"){ echo "selected='selected'"; } ?>>Tajikistan</option>
                            <option value="Tanzania, United Republic of" <?php if($userInfo['country'] == "Tanzania, United Republic of"){ echo "selected='selected'"; } ?>>Tanzania, United Republic of</option>
                            <option value="Thailand" <?php if($userInfo['country'] == "Thailand"){ echo "selected='selected'"; } ?>>Thailand</option>
                            <option value="Timor-leste" <?php if($userInfo['country'] == "Timor-leste"){ echo "selected='selected'"; } ?>>Timor-leste</option>
                            <option value="Togo" <?php if($userInfo['country'] == "Togo"){ echo "selected='selected'"; } ?>>Togo</option>
                            <option value="Tokelau" <?php if($userInfo['country'] == "Tokelau"){ echo "selected='selected'"; } ?>>Tokelau</option>
                            <option value="Tonga" <?php if($userInfo['country'] == "Tonga"){ echo "selected='selected'"; } ?>>Tonga</option>
                            <option value="Trinidad and Tobago" <?php if($userInfo['country'] == "Trinidad and Tobago"){ echo "selected='selected'"; } ?>>Trinidad and Tobago</option>
                            <option value="Tunisia" <?php if($userInfo['country'] == "Tunisia"){ echo "selected='selected'"; } ?>>Tunisia</option>
                            <option value="Turkey" <?php if($userInfo['country'] == "Turkey"){ echo "selected='selected'"; } ?>>Turkey</option>
                            <option value="Turkmenistan" <?php if($userInfo['country'] == "Turkmenistan"){ echo "selected='selected'"; } ?>>Turkmenistan</option>
                            <option value="Turks and Caicos Islands" <?php if($userInfo['country'] == "Turks and Caicos Islands"){ echo "selected='selected'"; } ?>>Turks and Caicos Islands</option>
                            <option value="Tuvalu" <?php if($userInfo['country'] == "Tuvalu"){ echo "selected='selected'"; } ?>>Tuvalu</option>
                            <option value="Uganda" <?php if($userInfo['country'] == "Uganda"){ echo "selected='selected'"; } ?>>Uganda</option>
                            <option value="Ukraine" <?php if($userInfo['country'] == "Ukraine"){ echo "selected='selected'"; } ?>>Ukraine</option>
                            <option value="United Arab Emirates" <?php if($userInfo['country'] == "United Arab Emirates"){ echo "selected='selected'"; } ?>>United Arab Emirates</option>
                            <option value="United Kingdom" <?php if($userInfo['country'] == "United Kingdom"){ echo "selected='selected'"; } ?>>United Kingdom</option>
                            <option value="United States" <?php if($userInfo['country'] == "United States"){ echo "selected='selected'"; } ?>>United States</option>
                            <option value="United States Minor Outlying Islands" <?php if($userInfo['country'] == "United States Minor Outlying Islands"){ echo "selected='selected'"; } ?>>United States Minor Outlying Islands</option>
                            <option value="Uruguay" <?php if($userInfo['country'] == "Uruguay"){ echo "selected='selected'"; } ?>>Uruguay</option>
                            <option value="Uzbekistan" <?php if($userInfo['country'] == "Uzbekistan"){ echo "selected='selected'"; } ?>>Uzbekistan</option>
                            <option value="Vanuatu" <?php if($userInfo['country'] == "Vanuatu"){ echo "selected='selected'"; } ?>>Vanuatu</option>
                            <option value="Venezuela" <?php if($userInfo['country'] == "Venezuela"){ echo "selected='selected'"; } ?>>Venezuela</option>
                            <option value="Viet Nam" <?php if($userInfo['country'] == "Viet Nam"){ echo "selected='selected'"; } ?>>Viet Nam</option>
                            <option value="Virgin Islands, British" <?php if($userInfo['country'] == "Virgin Islands, British"){ echo "selected='selected'"; } ?>>Virgin Islands, British</option>
                            <option value="Virgin Islands, U.S." <?php if($userInfo['country'] == "Virgin Islands, U.S."){ echo "selected='selected'"; } ?>>Virgin Islands, U.S.</option>
                            <option value="Wallis and Futuna" <?php if($userInfo['country'] == "Wallis and Futuna"){ echo "selected='selected'"; } ?>>Wallis and Futuna</option>
                            <option value="Western Sahara" <?php if($userInfo['country'] == "Western Sahara"){ echo "selected='selected'"; } ?>>Western Sahara</option>
                            <option value="Yemen" <?php if($userInfo['country'] == "Yemen"){ echo "selected='selected'"; } ?>>Yemen</option>
                            <option value="Zambia" <?php if($userInfo['country'] == "Zambia"){ echo "selected='selected'"; } ?>>Zambia</option>
                            <option value="Zimbabwe" <?php if($userInfo['country'] == "Zimbabwe"){ echo "selected='selected'"; } ?>>Zimbabwe</option>
                        </select>
                    </div>
                </div>
            </div>			
            <div class='col-lg-6'>
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </span>
                       <select name="province" id="province" class="form-control">
                        	<option value="">Select Province</option>
                            <option value="Eastern Cape" <?php if($userInfo['province'] == "Eastern Cape"){ echo "selected='selected'"; } ?>>Eastern Cape</option>
                            <option value="Free State" <?php if($userInfo['province'] == "Free State"){ echo "selected='selected'"; } ?>>Free State</option>
                            <option value="Gauteng" <?php if($userInfo['province'] == "Gauteng"){ echo "selected='selected'"; } ?>>Gauteng</option>
                            <option value="KwaZulu-Natal" <?php if($userInfo['province'] == "KwaZulu-Natal"){ echo "selected='selected'"; } ?>>KwaZulu-Natal</option>
                            <option value="Limpopo" <?php if($userInfo['province'] == "Limpopo"){ echo "selected='selected'"; } ?>>Limpopo</option>
                            <option value="Mpumalanga" <?php if($userInfo['province'] == "Mpumalanga"){ echo "selected='selected'"; } ?>>Mpumalanga</option>
                            <option value="Northern Cape" <?php if($userInfo['province'] == "Northern Cape"){ echo "selected='selected'"; } ?>>Northern Cape</option>
                            <option value="North West" <?php if($userInfo['province'] == "North West"){ echo "selected='selected'"; } ?>>North West</option>
                            <option value="Western Cape" <?php if($userInfo['province'] == "Western Cape"){ echo "selected='selected'"; } ?>>Western Cape</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">	
            <div class="col-lg-12">
                <div class="form-group pull-right">
                	<input type="hidden" name="user_id" value="<?php echo $userInfo['user_id']; ?>">
                    <button type="submit" id="register" class="btn btn-info">Update</button>
                </div>
            </div>
        </div>
    </div>
    <?php echo form_close(); ?>	
</div>
