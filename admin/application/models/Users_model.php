<?php
class Users_model extends CI_Model {
	private $_vehicleID;
    private $_userID;
    private $_userName;
    private $_firstName;
    private $_lastName;
    private $_email;
    private $_password;
    private $_contactNo;
    private $_address;
    private $_companyName;
    private $_verificationCode;
    private $_timeStamp;
    private $_status;
	private $_role;
	private $_country;
	private $_province;
	public function setVehicleID($vehicleID) {
        $this->_vehicleID = $vehicleID;
    }
    public function setUserID($userID) {
        $this->_userID = $userID;
    }
    public function setUserName($userName) {
        $this->_userName = $userName;
    }
    public function setFirstname($firstName) {
        $this->_firstName = $firstName;
    }
    public function setLastName($lastName) {
        $this->_lastName = $lastName;
    }
    public function setEmail($email) {
        $this->_email = $email;
    }
    public function setContactNo($contactNo) {
        $this->_contactNo = $contactNo;
    }
    public function setPassword($password) {
        $this->_password = $password;
    }
    public function setAddress($address) {
        $this->_address = $address;
    }
    public function setCompanyName($companyName) {
        $this->_companyName = $companyName;
    }
    public function setVerificationCode($verificationCode) {
        $this->_verificationCode = $verificationCode;
    }
    public function setTimeStamp($timeStamp) {
        $this->_timeStamp = $timeStamp;
    }
    public function setStatus($status) {
        $this->_status = $status;
    }
	public function setRole($role) {
        $this->_role = $role;
    }
	public function setCountry($country) {
        $this->_country = $country;
    }
	public function setProvince($province) {
        $this->_province = $province;
    }
    public function users_list(){
        $query = $this->db->get('users');
        return $query->result();
    }
	public function create($userData = array()) {
        $hash = $this->hash($this->_password);
        $data = array(
            'user_name' => $this->_userName,
            'first_name' => $this->_firstName,
            'last_name' => $this->_lastName,
            'email' => $this->_email,
            'password' => $hash,
            'contact_no' => $this->_contactNo,
            'address' => $this->_address,
            'company_name' => $this->_companyName,
			'country' => $this->_country,
			'province' => $this->_province,
            'verification_code' => $this->_verificationCode,
            'created_date' => $this->_timeStamp,
            'modified_date' => $this->_timeStamp,
            'status' => $this->_status,
			'role' => $this->_role
        );
        $this->db->insert('users', $data);
        if (!empty($this->db->insert_id()) && $this->db->insert_id() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
	public function getVehicleDetails() {
        $this->db->select(array('m.id as id', 'm.u_id', 'm.make', 'm.model', 'm.year', 'm.mmcode', 'm.vehicle_images', 'm.no_of_images', 'm.images_zip_path','m.images_zip_name', 'm.uploaded_date'));
        $this->db->from('vehicles as m');
        $this->db->where('m.u_id', $this->_userID);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }
	public function getUserDetails() {
        $this->db->select(array('m.id as user_id', 'CONCAT(m.first_name, " ", m.last_name) as full_name', 'm.first_name', 'm.last_name', 'm.email', 'm.contact_no', 'm.address', 'm.company_name', 'm.country', 'm.province', 'm.created_date', 'm.status', 'm.role'));
        $this->db->from('users as m');
        $this->db->where('m.id', $this->_userID);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return FALSE;
        }
    }
	public function update() {
        $data = array(
            'first_name' => $this->_firstName,
            'last_name' => $this->_lastName,
            'contact_no' => $this->_contactNo,
            'address' => $this->_address,
			'country' => $this->_country,
			'province' => $this->_province,
            'modified_date' => $this->_timeStamp,
            'company_name' => $this->_companyName,
            'modified_date' => $this->_timeStamp,
        );
        $this->db->where('id', $this->_userID);
        $msg = $this->db->update('users', $data);
        if ($msg == 1) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
	public function changePassword() {
        $hash = $this->hash($this->_password);
        $data = array(
            'password' => $hash,
        );
        $this->db->where('id', $this->_userID);
        $msg = $this->db->update('users', $data);
        if ($msg == 1) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
	public function updateForgotPassword() {
        $hash = $this->hash($this->_password);
        $data = array(
            'password' => $hash,
        );
        $this->db->where('email', $this->_email);
        $msg = $this->db->update('users', $data);
        if ($msg > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
	public function hash($password) {
        $hash = password_hash($password, PASSWORD_DEFAULT);
        return $hash;
    }
    public function verifyHash($password, $vpassword) {
        if (password_verify($password, $vpassword)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
	public function deleteVehicle(){
		$this->db->where('id', $this->_vehicleID);
		$msg = $this->db->delete('vehicles');
		if ($msg == 1) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
	public function deleteUser(){
		$this->db->where('id', $this->_userID);
		$msg = $this->db->delete('users');
		if ($msg == 1) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}