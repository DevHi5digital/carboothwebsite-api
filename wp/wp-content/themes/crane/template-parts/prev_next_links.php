<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );
/**
 * Template part for displaying prev \ next link of single posts.
 *
 * @package crane
 */


$current_page_options = crane_get_options_for_current_page();

if ( $current_page_options['show-prev-next-post'] ) {

	// Defaults.
	$taxonomy       = 'category';
	$title_tmpl     = '<span class="single-post-nav-title">%title</span>';
	$subtitle_tmpl  = '<span class="single-post-nav-date">%date</span>';
	$image_tmpl     = '%image';
	$show_type      = 'title-date-image';
	$text_prev_tmpl = '<span class="single-post-nav-title">' . __( 'Previous', 'crane' ) . '</span>';
	$text_next_tmpl = '<span class="single-post-nav-title">' . __( 'Next', 'crane' ) . '</span>';
	$show_text      = false;

	if ( is_single() && function_exists( 'is_product' ) && is_product() ) {
		$taxonomy      = 'product_cat';
		$subtitle_tmpl = '<span class="single-post-nav-price">%price</span>';
		$show_type     = 'title-price-image';
	}

	if ( ! empty( $current_page_options['show-prev-next-post-content'] ) ) {
		$show_type = esc_attr( $current_page_options['show-prev-next-post-content'] );
	}

	switch ( $show_type ) {
		case 'title-date-image':
		case 'title-price-image':
			break;
		case 'title-date':
		case 'title-price':
			$image_tmpl = '';
			break;
		case 'title-image':
			$subtitle_tmpl = '';
			break;
		case 'image-date':
		case 'image-price':
			$title_tmpl = '';
			break;
		case 'title':
			$subtitle_tmpl = '';
			$image_tmpl    = '';
			break;
		case 'date':
		case 'price':
			$title_tmpl = '';
			$image_tmpl = '';
			break;
		case 'image':
			$title_tmpl    = '';
			$subtitle_tmpl = '';
			break;
		case 'text':
			$show_text     = true;
			$title_tmpl    = '';
			$subtitle_tmpl = '';
			$image_tmpl    = '';
			break;
	}

	// Previous
	$template_prev = '<span class="single-post-nav-content">';
	$template_prev .= '<span class="single-post-nav-content-info">';
	if ( $title_tmpl || $subtitle_tmpl || $show_text ) {
		$template_prev .= $title_tmpl . $subtitle_tmpl;
		$template_prev .= $show_text ? $text_prev_tmpl : '';
	}
	$template_prev .= '</span>';
	$template_prev .= $image_tmpl;
	$template_prev .= '</span>';
	$template_prev .= '<span class="single-post-nav-arrow single-post-nav-arrow--prev"><span class="style-arrow"></span></span>';

	// Next
	$template_next = '<span class="single-post-nav-arrow single-post-nav-arrow--next"><span class="style-arrow"></span></span>';
	$template_next .= '<span class="single-post-nav-content">';
	$template_next .= $image_tmpl;
	$template_next .= '<span class="single-post-nav-content-info">';
	if ( $title_tmpl || $subtitle_tmpl || $show_text ) {
		$template_next .= $title_tmpl . $subtitle_tmpl;
		$template_next .= $show_text ? $text_next_tmpl : '';
	}
	$template_next .= '</span>';
	$template_next .= '</span>';


	/**
	 * Filters for template of previous post link.
	 *
	 * @since 1.5.8
	 *
	 * @param string $template_prev template.
	 */
	$template_prev = apply_filters( 'crane_filter_single_post_nav_prev', $template_prev );


	/**
	 * Filters for template of next post link.
	 *
	 * @since 1.5.8
	 *
	 * @param string $template_next template.
	 */
	$template_next = apply_filters( 'crane_filter_single_post_nav_next', $template_next );


	echo '<div class="crane-single-post-nav-wrapper crane-single-post-nav-type-' . $show_type . '">';
	previous_post_link(
		'%link',
		$template_prev,
		false,
		'',
		$taxonomy
	);
	next_post_link(
		'%link',
		$template_next,
		false,
		'',
		$taxonomy
	);
	echo '</div>';

}
