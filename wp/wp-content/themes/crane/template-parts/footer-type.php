<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );
/**
 * Template part for displaying footer.
 *
 * @package crane
 */


$preview = false;
if ( isset( $_GET['elementor-preview'] ) && isset( $_GET['crane_footer'] ) ) {
	$preview = true;
}
if ( is_preview() ) {
	$preview = true;
}


if ( $preview && 'crane_footer' === get_post_type() ) {

	while ( have_posts() ) : the_post();

		/**
		 * Takes content with other filters
		 *
		 * @param string
		 */
		$crane_footer = apply_filters( 'the_content', get_the_content() );

		echo '<footer class="footer">';

		/**
		 * Echo footer content
		 *
		 * @param string $crane_footer
		 */
		echo apply_filters( 'crane_footer_the_content', $crane_footer );

		echo '</footer>';

	endwhile; // End of the loop.

} else {

	/**
	 * Echo footer content
	 *
	 * @param string
	 */
	echo apply_filters( 'crane_footer_the_content', crane_get_footer_data( 'html' ) );

}
