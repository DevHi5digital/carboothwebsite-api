<div class="row page-content">
    <div class="col-lg-12">
        <h2>Vehicle's Details</h2>
        <?php if (validation_errors()) { ?>
            <div class="alert alert-danger">
                <?php echo validation_errors(); ?>
            </div>
        <?php } ?>
        <form method="POST" action="<?php echo site_url('vehicles/editVehicle');?>" enctype='multipart/form-data'>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-user-o"></i>
                        </span>
                        <input type="text" name="make" class="form-control" id="make" placeholder="Make" value="<?php print $vehicleInfo['make']; ?>">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-user-o"></i>
                        </span>
                        <input type="text" name="model" class="form-control" id="model" placeholder="Model" value="<?php print $vehicleInfo['model']; ?>">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">            			
            <div class="col-lg-6">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-envelope"></i>
                        </span>
                        <input type="text" name="year" class="form-control" id="year" placeholder="Year" value="<?php print $vehicleInfo['year']; ?>">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-phone"></i>
                        </span>
                        <input type="text" name="mmcode" class="form-control" id="mmcode" placeholder="M&M Code" value="<?php print $vehicleInfo['mmcode']; ?>">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">	
            <div class="col-lg-12">
                <div class="form-group">
                    <input type="file" name="vehicleimage[]" id="vehicle-image" accept=".png,.jpg,.jpeg,.gif" multiple>
                    <?php $bf_vimgs = explode(',',$vehicleInfo['vehicle_images']); $vimgs = array_filter($bf_vimgs); ?>
                    <?php if(!empty($vimgs)){ ?>
                        <div class="vehicle-imgs">
                        <?php foreach($vimgs as $vimg){ ?>
                            <div class="col-md-2" id="dimg_<?php echo $vehicleInfo['id']; ?>">
                                <div><img src="<?php echo base_url('assets/vimages/'.$vimg); ?>" width="200" height="200"></div>
                                <div><a href="javascript:void(0);" class="dltvimg btn btn-default" data-id="<?php print $vehicleInfo['id']; ?>" data-title="<?php print $vimg; ?>">Delete</a></div>
                            </div>
                        <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="row">	
            <div class="col-lg-12">
            	<div class="form-group pull-left">
                	<a href="<?php echo base_url('vehicles/vhllist/'.$vehicleInfo['u_id']); ?>" class="btn btn-info">Back</a>
                </div>
                <div class="form-group pull-right">
                	<input type="hidden" name="id" value="<?php echo $vehicleInfo['id']; ?>">
                	<input type="hidden" name="user_id" value="<?php echo $vehicleInfo['u_id']; ?>">
                    <input type="hidden" name="no_of_images" value="<?php echo $vehicleInfo['no_of_images']; ?>">
                    <input type="hidden" name="uploaded_date" value="<?php echo $vehicleInfo['uploaded_date']; ?>">
                    <input type="hidden" name="images_zip_path" value="<?php echo $vehicleInfo['images_zip_path']; ?>">
                    <input type="hidden" name="images_zip_name" value="<?php echo $vehicleInfo['images_zip_name']; ?>">
                    <button type="submit" id="update" class="btn btn-info">Update</button>
                </div>
            </div>
        </div>       
    </div>
    </form>
</div>
<script type="text/javascript">
jQuery(function($){
   $(".dltvimg").click(function(){
	  var getUrl = window.location;
	  var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
      var id = $(this).data("id");
	  var title = $(this).data("title");
      $.ajax({
        url: baseUrl+'/vehicles/deleteVehicleImg/',
		<?php /*?>url: "<?php echo base_url('vehicles/deleteVehicleImg/'); ?>",<?php */?>
        data: {'id': id,'title': title},
        type: "POST",
        success: function(status){
			if(status == 'ok'){
				//$('#dimg_'+id).remove();
                alert('The Image has been removed successfully.');
				window.location.reload();
            }else{
                alert('Some problem occurred, please try again.');
            }
        }
      });
   });
});
</script>