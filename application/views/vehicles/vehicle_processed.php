<div class="row page-content">
    <div class="col-lg-12">
        <h2>Vehicle Image Processed</h2>
        <?php $deliveryData   = $this->session->userdata('vehicleInfo'); ?>
        <div class="row">	
            <div class="col-lg-12">
                <div class="form-group">
                	<p><?php echo $this->session->userdata('vmake'); ?> - <?php echo $this->session->userdata('vmodel'); ?> - <?php echo $this->session->userdata('vyear'); ?></p>
                </div>
            </div>
        </div>
        <div class="row">	
            <div class="col-lg-12">
                <div class="form-group">
                    <p><strong>Download Vehicle Images Zip File: </strong><a href="<?php echo base_url('vehicles/download/'.$this->session->userdata['vehicleInfo']['id']) ?>"><i class="fa fa-file-archive-o" aria-hidden="true" style="color: #c00;font-size: 30px;"></i></a></p>
              <p><strong>Vehicle Images Zip File Send into Mail: </strong><a href="<?php echo base_url('vehicles/sendmail/'.$this->session->userdata['vehicleInfo']['id']) ?>" style="color: #c00;font-weight:bold;">Send Mail</a></p>
              </div>
            </div>
        </div>
        <div class="row">	
            <div class="col-lg-12">
                <div class="form-group">
                    <?php $bf_vimgs = explode(',',$this->session->userdata['vehicleInfo']['vehicle_images']); $vimgs = array_filter($bf_vimgs); ?>
                    <?php if(!empty($vimgs)){ ?>
                        <div class="vehicle-imgs">
                        <?php foreach($vimgs as $vimg){ ?>
                            <div class="col-md-2">
                                <div><img src="<?php echo base_url('assets/vimages/'.$vimg); ?>" width="500" height="500"></div>
                            </div>
                        <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>       
    </div>
</div>