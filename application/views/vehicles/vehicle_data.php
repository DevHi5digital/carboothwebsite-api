<div class="row page-content">
    <div class="col-lg-12">
        <h2>Vehicle Data</h2>
        <?php if (validation_errors()) { ?>
            <div class="alert alert-danger">
                <?php echo validation_errors(); ?>
            </div>
        <?php } ?>
        <form method="POST" action="<?php echo site_url('vehicles/vimguldvftn');?>" enctype='multipart/form-data'>
        <?php $deliveryData   = $this->session->userdata('vehicleInfo'); ?>
        <div class="row">	
            <div class="col-lg-12">
                <div class="form-group">
                	<p><?php echo $this->session->userdata('vmake'); ?> - <?php echo $this->session->userdata('vmodel'); ?> - <?php echo $this->session->userdata('vyear'); ?></p>
                    <input type="hidden" name="id" value="<?php echo $this->session->userdata['vehicleInfo']['id']; ?>">
                    <input type="hidden" name="make" value="<?php echo $this->session->userdata('vmake'); ?>">
                    <input type="hidden" name="model" value="<?php echo $this->session->userdata('vmodel'); ?>">
                    <input type="hidden" name="year" value="<?php echo $this->session->userdata('vyear'); ?>">
                    <input type="hidden" name="mmcode" value="<?php echo $this->session->userdata('vmmcode'); ?>">
                </div>
            </div>
        </div>
        <div class="row">	
            <div class="col-lg-12">
                <div class="form-group">
                    <input type="file" name="vehicleimage[]" id="vehicle-image" accept=".png,.jpg,.jpeg,.gif" multiple>
                    <?php $bf_vimgs = explode(',',$this->session->userdata['vehicleInfo']['vehicle_images']); $vimgs = array_filter($bf_vimgs); ?>
                    <?php if(!empty($vimgs)){ ?>
                        <div class="vehicle-imgs">
                        <?php foreach($vimgs as $vimg){ ?>
                            <div class="col-md-2">
                                <div><img src="<?php echo base_url('assets/vimages/'.$vimg); ?>" width="200" height="200"></div>
                                <p><a href="javascript:void(0);" class="dltvimg btn btn-default" data-id="<?php print $this->session->userdata['vehicleInfo']['id']; ?>" data-title="<?php print $vimg; ?>">Delete</a></p>
                            </div>
                        <?php } ?>
                        </div>
                    <?php } ?>
                    <div id="errorMsg"></div>
                </div>
            </div>
        </div>
        <div class="row">	
            <div class="col-lg-12">
                <div class="form-group pull-right">
                    <button type="submit" id="next" class="btn btn-info">Next</button>
                </div>
            </div>
        </div>       
    </div>
    </form>
</div>
<script type="text/javascript">
jQuery(function($){
   //$(".dltvimg").click(function(){
	$("body").on("click",".dltvimg", function(){
	  var getUrl = window.location;
	  var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
      var id = $(this).data("id");
	  var title = $(this).data("title");
      $.ajax({
        //url: baseUrl+'/vehicles/deleteVehicleImg/',
		url: baseUrl+'/deleteVehicleImg/',
		<?php /*?>url: "<?php echo base_url('vehicles/deleteVehicleImg/'); ?>",<?php */?>
        data: {'id': id,'title': title},
        type: "POST",
        success: function(status){
			if(status == 'ok'){
                alert('The Image has been removed successfully.');
				window.location.reload();
            }else{
                alert('Some problem occurred, please try again.');
            }
        }
      });
   });
});
</script>
<?php
$role = $this->session->userdata('role');
if($role != "administrator"){ ?>
<?php $exist_img_uploaded = count($bf_vimgs); ?>
<?php $no_of_available_token = $this->session->userdata('no_of_available_token'); ?>
<?php $total_img = $no_of_available_token - $exist_img_uploaded; ?>
<script>
var limit = <?php echo $total_img; ?>;
$(document).ready(function(){
    $('#vehicle-image').change(function(){
        var files = $(this)[0].files;
        if(files.length > limit){
			var errorMsg = "You can only upload a maximum of "+limit+" images.";
			var err = document.getElementById("errorMsg");
        	err.innerHTML = errorMsg;
            $('#vehicle-image').val('');
			setTimeout(function() {
			  $("#errorMsg").fadeOut().empty();
			}, 5000);
            return false;
        }else{
            return true;
        }
    });
});
</script>
<?php } ?>