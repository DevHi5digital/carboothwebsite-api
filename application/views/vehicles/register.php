<div class="row page-content">
    <div class="col-lg-12">
        <h2>Vehicle Data</h2>
        <?php if (validation_errors()) { ?>
            <div class="alert alert-danger">
                <?php echo validation_errors(); ?>
            </div>
        <?php } ?>
        <form method="POST" action="<?php echo site_url('vehicles/actionCreate');?>" enctype='multipart/form-data'>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-user-o"></i>
                        </span>
                        <input type="text" name="make" class="form-control" id="make" placeholder="Make">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-user-o"></i>
                        </span>
                        <input type="text" name="model" class="form-control" id="model" placeholder="Model">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">            			
            <div class="col-lg-6">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-envelope"></i>
                        </span>
                        <input type="text" name="year" class="form-control" id="year" placeholder="Year">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-phone"></i>
                        </span>
                        <input type="text" name="mmcode" class="form-control" id="mmcode" placeholder="M&M Code">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">	
            <div class="col-lg-12">
                <div class="form-group pull-right">
                    <button type="submit" id="next" class="btn btn-info">Next</button>
                </div>
            </div>
        </div>       
    </div>
    </form>
</div>
