<p align="center"><strong><h3>Privacy Policy For CarBooth Mobile Application</h3></strong> <br>
  Effective Date: 02/16/2021 <br>
  Applicable To The Following  Mobile Application: <br>
  CarBooth <br>
  <strong><br>
    Article 1 - DEFINITIONS:</strong> <br>
  a) APPLICABLE MOBILE APPLICATION: This Privacy Policy will  refer to and be applicable to the Mobile App listed above, which shall  hereinafter be referred to as &quot;Mobile App.&quot; <br>
  b) EFFECTIVE DATE: &quot;Effective Date&quot; means the  date this Privacy Policy comes into force and effect. <br>
  c) PARTIES: The parties to this privacy policy are the  following data controller: CarBooth (&quot;Data Controller&quot;) and you, as  the user of this Mobile App. Hereinafter, the parties will individually be  referred to as &quot;Party&quot; and collectively as &quot;Parties.&quot; <br>
  d) DATA CONTROLLER: Data Controller is the publisher,  owner, and operator of the Mobile App and is the Party responsible for the  collection of information described herein. Data Controller shall be referred  to either by Data Controller's name or &quot;Data Controller,&quot; as listed  above. If Data Controller or Data Controller's property shall be referred to  through first-person pronouns, it shall be through the use of the following:  us, we, our, ours, etc. <br>
  e) YOU: Should you agree to this Privacy Policy and  continue your use of the Mobile App, you will be referred to herein as either  you, the user, or if any second-person pronouns are required and applicable,  such pronouns as 'your&quot;, &quot;yours&quot;, etc. <br>
  f) SERVICES: &quot;Services&quot; means any services that  we make available for sale on the Mobile App. <br>
  g) PERSONAL DATA: &quot;Personal DATA&quot; means personal  data and information that we obtain from you in connection with your use of the  Mobile App that is capable of identifying you in any manner. <br>
  <strong><br>
    Article 2 - GENERAL INFORMATION:</strong> <br>
  This privacy policy (hereinafter &quot;Privacy  Policy&quot;) describes how we collect and use the Personal Data that we  receive about you, as well as your rights in relation to that Personal Data,  when you visit our Mobile App or use our Services. <br>
  This Privacy Policy does not cover any information that we  may receive about you through sources other than the use of our Mobile App. The  Mobile App may link out to other websites or mobile applications, but this  Privacy Policy does not and will not apply to any of those linked websites or  applications. <br>
  We are committed to the protection of your privacy while  you use our Mobile App. <br>
  By continuing to use our Mobile App, you acknowledge that  you have had the chance to review and consider this Privacy Policy, and you acknowledge  that you agree to it. This means that you also consent to the use of your  information and the method of disclosure as described in this Privacy Policy.  If you do not understand the Privacy Policy or do not agree to it, then you  agree to immediately cease your use of our Mobile App. <br>
  <strong><br>
    Article 3 -CONTACT AND DATA PROTECTION OFFICER:</strong> <br>
  The Party responsible for the processing of your personal  data is as follows: CarBooth. The Data Controller may be contacted as follows: <br>
  19 Witzenberg street, Durbanville hills<br>
  Durbanville, Cape Town, 75500<br>
  South Africa <br>
  The Data Controller and operator of the Mobile App are one  and the same. <br>
  The Data Protection Officer is as follows: Gaven Osmond.  The Data Protection Officer may be contacted as follows: <br>
  19 Witzenberg street, Durbanville hills<br>
  Durbanville, Cape Town, 75500<br>
  South Africa <br>
  <strong><br>
    Article 4 - LOCATION:</strong> <br>
  The location where the data processing activities take  place is as follows: <br>
  19 Witzenberg street, Durbanville hills<br>
  Durbanville, Cape Town, 75500<br>
  South Africa <br>
  <strong><br>
    Article 5 - MODIFICATIONS AND REVISIONS:</strong> <br>
  We reserve the right to modify, revise, or otherwise amend  this Privacy Policy at any time and in any manner. If we do so, however, we  will notify you and obtain your consent to the change in processing. Unless we  specifically obtain your consent, any changes to the Privacy Policy will only  impact the information collected on or after the date of the change. It is also  your responsibility to periodically check this page for any such modification,  revision or amendment. <br>
  <strong><br>
    Article 6 - THE PERSONAL DATA WE RECEIVE FROM YOU:</strong> <br>
  Depending on how you use our Mobile App, you will be  subject to different types of Personal Data collected and different manners of  collection: <br>
  <strong>a)</strong> <strong>Registered users:</strong> You, as a user  of the Mobile App, may be asked to register in order to use the Mobile App or  to purchase the Services available for sale. <br>
  During the process of your registration,  we will collect some of the following Personal Data from you through your  voluntary disclosure: <br>
  title, name, house address, e-mail  address, phone number, bank details, credit card number <br>
  Personal Data may be asked for in relation  to: <br>
  I) Interaction with our representatives in  any way <br>
  II) making purchases <br>
  III) receiving notifications by text  message or email about marketing <br>
  IV) receiving general emails from us <br>
  V) commenting on our content or other  user-generated content on our Mobile App, such as blogs, articles, photographs  or videos, or participating in our forums, bulletin boards, chat rooms or other  similar features <br>
  By undergoing the registration process,  you consent to us collecting your Personal Data, including the Personal Data  described in this clause, as well as storing, using or disclosing your Personal  Data in accordance with this Privacy Policy. <br>
  <strong>b)</strong> <strong>Unregistered users:</strong> If you are a  passive user of the Mobile App and do not register for any purchases or other  service, you may still be subject to certain passive data collection  (&quot;Passive Data Collection&quot;). Such Passive Data Collection may include  through cookies, as described below, IP address information, location  information, and certain browser data, such as history and/or session  information. <br>
  <strong>c)</strong> <strong>All users:</strong> The Passive Data  Collection that applies to Unregistered users shall also apply to all other  users and/or visitors of our Mobile App. <br>
  <strong>d)</strong> <strong>Sales &amp; Billing Information:</strong> In order to purchase any of the services on the Mobile App, you will be asked  to provide certain credit information, billing address information, and  possibly additional specific information so that you may be properly charged for  your purchases. This payment and billing information will not be stored and  will be used exclusively to assist with your one-time purchase. <br>
  <strong>e)</strong> <strong>Email Marketing:</strong> You may be asked  to provide certain Personal Data, such as your name and email address, for the  purpose of receiving email marketing communications. This information will only  be obtained through your voluntary disclosure and you will be asked to  affirmatively opt-in to email marketing communications. <br>
  <strong>f)</strong> <strong>Content Interaction:</strong> Our Mobile  App may allow you to comment on the content that we provide or the content that  other users provide, such as blogs, multimedia, or forum posts. If so, we may  collect some Personal Data from you at that time, such as, but not limited to,  username or email address. <br>
  <strong><br>
    Article 7 - THE PERSONAL DATA WE RECEIVE AUTOMATICALLY:</strong> <br>
  <strong>Cookies:</strong> We may collect information from you  through automatic tracking systems (such as information about your browsing  preferences) as well as through information that you volunteer to us (such as  information that you provide during a registration process or at other times  while using the Mobile App, as described above). <br>
  For example, we use cookies to make your browsing  experience easier and more intuitive: cookies are small strings of text used to  store some information that may concern the user, his or her preferences or the  device they are using to access the internet (such as a computer, tablet, or  mobile phone). Cookies are mainly used to adapt the operation of the site to  your expectations, offering a more personalized browsing experience and  memorizing the choices you made previously. <br>
  A cookie consists of a reduced set of data transferred to  your browser from a web server and it can only be read by the server that made  the transfer. This is not executable code and does not transmit viruses. <br>
  Cookies do not record or store any Personal Data. If you  want, you can prevent the use of cookies, but then you may not be able to use  our Mobile App as we intend. To proceed without changing the options related to  cookies, simply continue to use our Mobile App. <br>
  <strong>Technical cookies:</strong> Technical cookies, which can  also sometimes be called HTML cookies, are used for navigation and to  facilitate your access to and use of the site. They are necessary for the  transmission of communications on the network or to supply services requested  by you. The use of technical cookies allows the safe and efficient use of the  site. <br>
  You can manage or request the general  deactivation or cancelation of cookies through your browser. If you do this  though, please be advised this action might slow down or prevent access to some  parts of the site. <br>
  Cookies may also be retransmitted by an  analytics or statistics provider to collect aggregated information on the  number of users and how they visit the Mobile App. These are also considered  technical cookies when they operate as described. <br>
  Temporary session cookies are deleted  automatically at the end of the browsing session - these are mostly used to  identify you and ensure that you don't have to log in each time - whereas  permanent cookies remain active longer than just one particular session. <br>
  <strong>Third-party cookies:</strong> We may also utilize  third-party cookies, which are cookies sent by a third-party to your computer.  Permanent cookies are often third-party cookies. The majority of third-party  cookies consist of tracking cookies used to identify online behavior,  understand interests and then customize advertising for users. <br>
  Third-party analytical cookies may also be  installed. They are sent from the domains of the aforementioned third parties  external to the site. Third-party analytical cookies are used to detect  information on user behavior on our Mobile App. This place anonymously, in  order to monitor the performance and improve the usability of the site.  Third-party profiling cookies are used to create profiles relating to users, in  order to propose advertising in line with the choices expressed by the users  themselves. <br>
  <strong>Profiling cookies:</strong> We may also use profiling  cookies, which are those that create profiles related to the user and are used  in order to send advertising to the user's browser. <br>
  When these types of cookies are used, we  will receive your explicit consent. <br>
  <strong>Support in configuring your browser:</strong> You can manage cookies through  the settings of your browser on your device. However, deleting cookies from  your browser may remove the preferences you have set for this Mobile App. <br>
  For further information and support, you  can also visit the specific help page of the web browser you are using: <br>
  - Internet Explorer:  http://windows.microsoft.com/en-us/windows-vista/block-or-allow-cookies <br>
  - Firefox:  https://support.mozilla.org/en-us/kb/enable-and-disable-cookies-website-preferences <br>
  - Safari:  http://www.apple.com/legal/privacy/ <br>
  - Chrome:  https://support.google.com/accounts/answer/61416?hl=en <br>
  - Opera:  http://www.opera.com/help/tutorials/security/cookies/ <br>
  <strong>Log Data:</strong> Like all websites and mobile  applications, this Mobile App also makes use of log files that store automatic  information collected during user visits. The different types of log data could  be as follows: <br>
  - internet protocol (IP) address;<br>
  - type of browser and device parameters used to connect to the Mobile App;<br>
  - name of the Internet Service Provider (ISP);<br>
  - date and time of visit;<br>
  - web page of origin of the user (referral) and exit;<br>
  - possibly the number of clicks. <br>
  The aforementioned information is processed in an automated  form and collected in an exclusively aggregated manner in order to verify the  correct functioning of the site, and for security reasons. This information  will be processed according to the legitimate interests of the Data Controller. <br>
  For security purposes (spam filters, firewalls, virus  detection), the automatically recorded data may also possibly include Personal  Data such as IP address, which could be used, in accordance with applicable  laws, in order to block attempts at damage to the Mobile App or damage to other  users, or in the case of harmful activities or crime. Such data are never used  for the identification or profiling of the user, but only for the protection of  the Mobile App and our users. Such information will be treated according to the  legitimate interests of the Data Controller. <br>
  <strong><br>
    Article 8 - THIRD PARTIES:</strong> <br>
  We may utilize third-party service providers  (&quot;Third-Party Service Providers&quot;), from time to time or all the time,  to help us with our Mobile App, and to help serve you. <br>
  We may use Third-Party Service Providers  to assist with information storage (such as cloud storage). <br>
  We may provide some of your Personal Data  to Third-Party Service Providers in order to help us track usage data, such as  referral websites, dates and times of page requests, etc. We use this  information to understand patterns of usage of, and to improve, the Mobile App. <br>
  We may use Third-Party Service Providers  to host the Mobile App. In this instance, the Third-Party Service Provider will  have access to your Personal Data. <br>
  We may use Third-Party Service Providers  to fulfill orders in relation to the Mobile App. <br>
  We may allow third parties to advertise on  the Mobile App. These third parties may use cookies in connection with their  advertisements (see the &quot;Cookies&quot; clause in this Privacy Policy). <br>
  Your Personal Data will not be sold or  otherwise transferred to other third parties without your approval. <br>
  Notwithstanding the other provisions of  this Privacy Policy, we may provide your Personal Data to a third party or to  third parties in order to protect the rights, property or safety, of us, our  customers or third parties, or as otherwise required by law. <br>
  We will not knowingly share your Personal  Data with any third parties other than in accordance with this Privacy Policy. <br>
  If your Personal Data might be provided to  a third party in a manner that is other than as explained in this Privacy  Policy, you will be notified. You will also have the opportunity to request  that we not share that information. <br>
  In general, you may request that we do not  share your Personal Data with third parties. Please contact us via email, if  so. Please be advised that you may lose access to certain services that we rely  on third-party providers for. <br>
  <strong><br>
    Article 9 - HOW PERSONAL DATA IS STORED:</strong> <br>
  We use secure physical and digital systems to store your  Personal Data when appropriate. We ensure that your Personal Data is protected  against unauthorized access, disclosure, or destruction. <br>
  Please note, however, that no system involving the  transmission of information via the internet, or the electronic storage of  data, is completely secure. However, we take the protection and storage of your  Personal Data very seriously. We take all reasonable steps to protect your  Personal Data. <br>
  Personal Data is stored throughout your relationship with  us. We delete your Personal Data upon request for cancelation of your account  or other general request for the deletion of data. <br>
  In the event of a breach of your Personal Data, you will be  notified in a reasonable time frame, but in no event later than two weeks, and  we will follow all applicable laws regarding such breach. <br>
  <strong><br>
    Article 10 - PURPOSES OF PROCESSING OF PERSONAL DATA:</strong> <br>
  We primarily use your Personal Data to help us provide a  better experience for you on our Mobile App and to provide you the services  and/or information you may have requested, such as use of our Mobile App. <br>
  Information that does not identify you personally, but that  may assist in providing us broad overviews of our customer base, will be used  for market research or marketing efforts. Such information may include, but is  not limited to, interests based on your cookies. <br>
  Personal Data that may be considering identifying may be  used for the following: <br>
  a) Improving your personal user experience <br>
  b) Communicating with you about your user  account with us <br>
  c) Marketing and advertising to you,  including via email <br>
  d) Fulfilling your purchases <br>
  e) Advising you about updates to the  Mobile App or related Items <br>
  <strong><br>
    Article 11 - DISCLOSURE OF PERSONAL DATA:</strong> <br>
  Although our policy is to maintain the privacy of your  Personal Data as described herein, we may disclose your Personal Data if we  believe that it is reasonable to do so in certain cases, in our sole and  exclusive discretion. Such cases may include, but are not limited to: <br>
  a) To satisfy any local, state, or Federal  laws or regulations <br>
  b) To respond to requests, such discovery,  criminal, civil, or administrative process, subpoenas, court orders, or writs  from law enforcement or other governmental or legal bodies <br>
  c) To bring legal action against a user  who has violated the law or violated the terms of use of our Mobile App <br>
  d) As may be necessary for the operation  of our Mobile App <br>
  e) To generally cooperate with any lawful  investigation about our users <br>
  f) If we suspect any fraudulent activity  on our Mobile App or if we have noticed any activity which may violate our  terms or other applicable rules <br>
  <strong><br>
    Article 12 - OPTING OUT OF TRANSMITTALS FROM US:</strong> <br>
  From time to time, we may send you informational or marketing  communications related to our Mobile App such as announcements or other  information. If you wish to opt-out of such communications, you may contact the  following email: optout@carbooth.co.za. You may also click the opt-out link  which will be provided at the bottom of any and all such communications. <br>
  Please be advised that even though you may opt-out of such  communications, you may still receive information from us that is specifically  about your use of our Mobile App or about your account with us. <br>
  By providing any Personal Data to us, or by using our  Mobile App in any manner, you have created a commercial relationship with us.  As such, you agree that any email sent from us or third-party affiliates, even  unsolicited email, shall specifically not be considered SPAM, as that term is  legally defined. <br>
  <strong><br>
    Article 13 - MODIFYING, DELETING, AND ACCESSING YOUR INFORMATION:</strong> <br>
  If you wish to modify or delete any information we may have  about you, or you wish to simply access any information we have about you, you  may do so from your account settings page. <br>
  <strong><br>
    Article 14 - ACCEPTANCE OF RISK:</strong> <br>
  By continuing to our Mobile App in any manner, use the  Product, you manifest your continuing asset to this Privacy Policy. You further  acknowledge, agree and accept that no transmission of information or data via  the internet is not always completely secure, no matter what steps are taken.  You acknowledge, agree and accept that we do not guarantee or warrant the  security of any information that you provide to us, and that you transmit such  information at your own risk. <br>
  <strong><br>
    Article 15 - YOUR RIGHTS:</strong> <br>
  You have many rights in relation to your Personal Data.  Specifically, your rights are as follows: <br>
  - the right to be informed about the  processing of your Personal Dat <br>
  - the right to have access to your  Personal Data <br>
  - the right to update and/or correct your  Personal Data <br>
  - the right to portability of your  Personal Data <br>
  - the right to oppose or limit the  processing of your Personal Data <br>
  - the right to request that we stop  processing and delete your Personal Data <br>
  - the right to block any Personal Data  processing in violation of any applicable law <br>
  - the right to launch a complaint with the  Federal Trade Commission (FTC) in the United States or applicable data  protection authority in another jurisdiction <br>
  Such rights can all be exercised by contacting us at the  relevant contact information listed in this Privacy Policy. <br>
  <strong><br>
    Article 16 - CONTACT INFORMATION:</strong> <br>
  If you have any questions about this Privacy Policy or the  way we collect information from you, or if you would like to launch a complaint  about anything related to this Privacy Policy, you may contact us at the  following email address: info@carbooth.co.za. </p>