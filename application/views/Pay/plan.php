<div class="container">
  <div>
  	<p>Hi, <?php echo $user['name'].' '.$user['surname']; ?></p>
    <?php
	$databaseDateTime = $tokenData['lastdate'];
	$formatedDateTime = date('d/m/Y h:i A', strtotime($databaseDateTime));
	if(!empty($tokenData['tokens'])){ $no_of_available_tokens = $tokenData['tokens']; }else{  $no_of_available_tokens = 0; }
	?>
	<p>Currently, You have <?php echo $no_of_available_tokens; ?> tokens available on Date <?php echo $formatedDateTime; ?>. Tokens cost is R25 each.</p>
  </div>
  <div class="regisFrm">
     <?php echo form_open('Pay/confirm'); ?>
      <div class="form-group">
      <?php
	  	foreach($token as $tcdata){
		  $token_cost = $tcdata->token_cost;
	  	}
		?>
      	<label>Buy more Tokens</label>
      	<select name="plan">
            <option value="15-<?php echo $token_cost * 15; ?>" style="font-size:12px;">15 Tokens - R<?php echo $token_cost * 15; ?> (R25.00 per photo) </option>
          	<option value="30-<?php echo $token_cost * 30 - 150; ?>" style="font-size:13px;">30 Tokens - R<?php echo $token_cost * 30 - 150; ?> (R20.00 per photo) </option>
          	<option value="60-<?php echo $token_cost * 60 - 720; ?>" style="font-size:12px;">60 Tokens - R<?php echo $token_cost * 60 - 720; ?> (R13 per photo) </option>
          	<option value="100-<?php echo $token_cost * 100 - 1600; ?>" style="font-size:12px;">100 Tokens - R<?php echo $token_cost * 100 - 1600 ?> (R9 per photo) </option>
        </select>
      </div>
      <div class="send-button">
        <input type="submit" name="planSubmit" value="Submit" class="btn btn-info">
      </div>
    <?php echo form_close(); ?>	
  </div>
</div>