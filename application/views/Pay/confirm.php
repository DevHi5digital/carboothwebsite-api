<?php 
$plandata = explode('-',$plan);
$no_of_tokens = $plandata[0];
$total_token_amount = $plandata[1];
if(!empty($tokenData['tokens'])){ $no_of_available_tokens = $tokenData['tokens']; }else{  $no_of_available_tokens = 0; }
?>
<?php
function generateSignature($data, $passPhrase = null) {
    $pfOutput = '';
    foreach( $data as $key => $val ) {
        if(!empty($val)) {
            $pfOutput .= $key .'='. urlencode( trim( $val ) ) .'&';
        }
    }
    $getString = substr( $pfOutput, 0, -1 );
    if( $passPhrase !== null ) {
        $getString .= '&passphrase='. urlencode( trim( $passPhrase ) );
    }
    return md5( $getString );
}
?>
<div class="container">
  <div class="regisFrm">
  <h1>Summary</h1>
  <div><strong>First Name:</strong> <?php echo $user['name']; ?></div>
  <div><strong>Last Name:</strong> <?php echo $user['surname']; ?></div>
  <div><strong>Email Address:</strong> <?php echo $user['email']; ?></div>
  <div><strong>Available Tokens:</strong> <?php echo $no_of_available_tokens; ?></div>
  <div><strong>Buy Tokens:</strong> <?php echo $no_of_tokens; ?></div>
  <div><strong>Buy Tokens Amount:</strong> <?php echo $total_token_amount; ?></div>
     <?php
	$cartTotal = $total_token_amount;
	$data = array(	
//		'merchant_id' => '10000100',
//		'merchant_key' => '46f0cd694581a',	
		'merchant_id' => '16933919',
		'merchant_key' => 't8o839ky8n4iq',
		'return_url' => site_url().'Pay/success',
		'cancel_url' => site_url().'Pay/cancel',
		'notify_url' => site_url().'Pay/notify',
		'name_first' => $user['name'],
		'name_last'  => $user['surname'],
		'email_address'=> $user['email'],
		'm_payment_id' => '1234',
		'amount' => number_format( sprintf( '%.2f', $cartTotal ), 2, '.', '' ),
		'item_name' => $no_of_tokens.' Tokens'
	);
	$signature = generateSignature($data);
	$data['signature'] = $signature;
	$testingMode = true;
	$pfHost = $testingMode ? 'www.payfast.co.za' : 'www.payfast.co.za';
	$htmlForm = '<form action="https://'.$pfHost.'/eng/process" method="post">';
	foreach($data as $name=> $value)
	{
		$htmlForm .= '<input name="'.$name.'" type="hidden" value="'.$value.'" />';
	}
	$htmlForm .= '<input type="submit" value="Confirm & Pay Now" />';
	$htmlForm .= '<a href="'.base_url('Pay/plan').'" class="btn btn-danger">Cancel</a>';
	$htmlForm .= '</form>';
	echo $htmlForm;
	?>
  </div>
</div>