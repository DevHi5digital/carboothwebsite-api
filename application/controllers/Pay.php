<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Pay extends CI_Controller { 
    public function __construct() { 
        parent::__construct();
        $this->load->model('Pay_model', 'pay');
		$this->load->model('Users_model', 'users');
		$this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->library('form_validation');
		$this->load->library('session');
        /*$this->isUserLoggedIn = $this->session->userdata('isUserLoggedIn'); 
		if ($this->session->userdata('ci_session_key_generate') == FALSE) { 
            redirect('signin');
        }*/
		if ($this->session->userdata('role') == "administrator") {
			 redirect('profile');
		}
    }
	public function index($UsrID = NULL){
		if(!empty($UsrID)) {
			$this->users->setUserID($UsrID);
			$chk_result = $this->users->getUserDetails();
			if ($chk_result == FALSE) {
				redirect('signin');
			}else{
				$this->session->set_userdata('userId', $UsrID);
				$uid = $this->session->userdata('userId');
			}
		}else{
			$con = array( 
				'id' => $this->session->userdata('userId') 
			);
			$sessionArray = $this->session->userdata('ci_seesion_key');
			$uid = $sessionArray['user_id'];
		}
		if(!empty($uid)) {
			redirect('Pay/plan');
		}else{
			redirect('signin');
		}
	}
	/*public function index(){ 
        redirect('Pay/plan'); 
    }*/
    public function plan(){ 
        $data = array();
		$con = array( 
			'id' => $this->session->userdata('userId') 
		);
		$sessionArray = $this->session->userdata('ci_seesion_key');
		if(!empty($sessionArray['user_id'])){
			$uid = $sessionArray['user_id'];
		}else{
			$uid = $this->session->userdata('userId');
		}
		if(empty($uid)) {
			redirect('signin');
		}
		$this->users->setUserID($uid);
		$this->pay->setUserID($uid);
		$data['user'] = $this->users->getUserDetails();
		$data['token'] = $this->pay->tokencost_list(); 
		$data['tokenData'] = $this->pay->getTokenDetails(); 
		$this->load->view('templates/header', $data);
		$this->load->view('Pay/plan', $data); 
		$this->load->view('templates/footer');
    }
	 public function confirm(){ 
        $data = array(); 
		$con = array( 
			'id' => $this->session->userdata('userId') 
		);
		$sessionArray = $this->session->userdata('ci_seesion_key');
		if(!empty($sessionArray['user_id'])){
			$uid = $sessionArray['user_id'];
		}else{
			$uid = $this->session->userdata('userId');
		}
		if(empty($uid)) {
			redirect('signin');
		}
		$this->users->setUserID($uid);
		$this->pay->setUserID($uid);
		$data['user'] = $this->users->getUserDetails();
		$data['token'] = $this->pay->tokencost_list();
		$data['tokenData'] = $this->pay->getTokenDetails();  
		$data['plan'] = $this->input->post('plan');
		$plandata = explode('-',$data['plan']);
		$amount = $plandata[1];
		$tokens = $plandata[0];
    	$this->session->set_userdata('total_amount', $amount);
		$this->session->set_userdata('no_of_tokens', $tokens);
		$this->load->view('templates/header', $data);
		$this->load->view('Pay/confirm', $data); 
		$this->load->view('templates/footer');
    }
	public function notify(){ 
        $data = array(); 
		$con = array( 
			'id' => $this->session->userdata('userId') 
		);
		$sessionArray = $this->session->userdata('ci_seesion_key');
		if(!empty($sessionArray['user_id'])){
			$uid = $sessionArray['user_id'];
		}else{
			$uid = $this->session->userdata('userId');
		}
		if(empty($uid)) {
			redirect('signin');
		}
		$this->users->setUserID($uid);
		$this->pay->setUserID($uid);
		$data['user'] = $this->users->getUserDetails();
		$data['token'] = $this->pay->tokencost_list(); 
		$this->load->view('templates/header', $data);
		$this->load->view('Pay/notify', $data); 
		$this->load->view('templates/footer');
    }
	public function cancel(){ 
        $data = array(); 
		if($this->session->userdata('userId')){
			$uid = $this->session->userdata('userId');
		}else{
			$con = array( 
				'id' => $this->session->userdata('userId') 
			);
			$sessionArray = $this->session->userdata('ci_seesion_key');
			$uid = $sessionArray['user_id'];
		}
		$this->users->setUserID($uid);
		$this->pay->setUserID($uid);
		$data['user'] = $this->users->getUserDetails();
		$data['token'] = $this->pay->tokencost_list(); 
		$this->load->view('templates/header', $data);
		$this->load->view('Pay/cancel', $data); 
		$this->load->view('templates/footer');
    }
	public function success(){ 
        $data = array(); 
		$con = array( 
			'id' => $this->session->userdata('userId') 
		);
		$sessionArray = $this->session->userdata('ci_seesion_key');
		if(!empty($sessionArray['user_id'])){
			$uid = $sessionArray['user_id'];
		}else{
			$uid = $this->session->userdata('userId');
		}
		if(empty($uid)) {
			redirect('signin');
		}
		$this->users->setUserID($uid);
		$this->pay->setUserID($uid);
		$data['user'] = $this->users->getUserDetails();
		$data['token'] = $this->pay->tokencost_list(); 
		$data['tokenData'] = $this->pay->getTokenDetails();
		$total_amount = $this->session->userdata('total_amount');
		$no_of_tokens = $this->session->userdata('no_of_tokens');
		$date_time =  date('Y-m-d H:i:s');
		$token_id = $data['tokenData']['id'];
		$no_of_updated_token = $data['tokenData']['tokens'] + $no_of_tokens;
		if($this->session->userdata('total_amount') && $this->session->userdata('no_of_tokens')){
			$this->pay->setTotalAmount($total_amount);
			$this->pay->setNoOfTokens($no_of_tokens);
			$this->pay->setDateTime($date_time);
			$this->pay->setTokenID($token_id);
			$this->pay->setNoOfUpdatedToken($no_of_updated_token);
			$this->pay->setLastdate($date_time);
			$tp = $this->pay->tokensPayments();
			$ut = $this->pay->updateTokens();
		}
		$data['updatedTokenData'] = $this->pay->getTokenDetails();
		$data['paymentTokenData'] = $this->pay->getPaymentTokenDetails();
		$this->load->view('templates/header', $data);
		$this->load->view('Pay/success', $data); 
		$this->load->view('templates/footer');
		$this->session->unset_userdata('total_amount');
		$this->session->unset_userdata('no_of_tokens');
    }
}