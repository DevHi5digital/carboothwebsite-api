<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Privacypolicy extends CI_Controller {
	public function index()
	{
		$this->load->helper('url');
		$this->load->view('templates/header');
		$this->load->view('privacypolicy'); 
		$this->load->view('templates/footer');
	}
}
