<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Auth extends MY_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('Auth_model', 'auth');
		$this->load->model('Pay_model', 'pay');
		$this->load->model('Users_model', 'users');
        $this->load->model('Mail', 'mail');
        $this->load->library('form_validation');
		$this->load->library('session');
    }
    public function index() {
        if ($this->session->userdata('ci_session_key_generate') == FALSE) {
            redirect('signin');
        } else {
            $data = array();
            $sessionArray = $this->session->userdata('ci_seesion_key');
            $this->auth->setUserID($sessionArray['user_id']);
            $data['userInfo'] = $this->auth->getUserDetails();
            $this->page_construct('auth/index', $data);
        }
    }
    public function register() {
		if ($this->session->userdata('ci_session_key_generate') == TRUE) {
            redirect('profile');
        }
        $this->page_construct('auth/register');
    }
    public function edit() {
        if ($this->session->userdata('ci_session_key_generate') == FALSE) {
            redirect('signin');
        } else {
            $data = array();
            $sessionArray = $this->session->userdata('ci_seesion_key');
            $this->auth->setUserID($sessionArray['user_id']);
            $data['userInfo'] = $this->auth->getUserDetails();
            $this->page_construct('auth/edit', $data);
        }
    }
    public function login() {
		if ($this->session->userdata('ci_session_key_generate') == TRUE) {
            redirect('profile');
        }
        if (!empty($this->input->get('usid'))) {
            $verificationCode = urldecode(base64_decode($this->input->get('usid')));
            $this->auth->setVerificationCode($verificationCode);
            $this->auth->activate();
        }
        $this->page_construct('auth/login');
    }
    public function changepwd() {
        if ($this->session->userdata('ci_session_key_generate') == FALSE) {
            redirect('signin');
        } else {
            $this->page_construct('auth/changepwd');
        }
    }
    public function forgotpassword() {
        if ($this->session->userdata('ci_session_key_generate') == TRUE) {
            redirect('profile');
        } else {
            $this->page_construct('auth/forgotpwd');
        }
    }
    public function actionCreate() {
        $this->form_validation->set_rules('first_name', 'First Name', 'required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('contact_no', 'Contact No', 'required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]');
        $this->form_validation->set_rules('confirm_password', 'Password Confirmation', 'trim|required|matches[password]');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('company_name', 'Company Name', 'required');
		$this->form_validation->set_rules('country', 'Country', 'required');
		$this->form_validation->set_rules('province', 'Province', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->register();
        } else {
            $firstName = $this->input->post('first_name');
            $lastName = $this->input->post('last_name');
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $contactNo = $this->input->post('contact_no');
            $companyName = $this->input->post('company_name');
            $address = $this->input->post('address');
			$country = $this->input->post('country');
			$province = $this->input->post('province');
            $timeStamp = time();
            $status = 0;
            $verificationCode = uniqid();
            $verificationLink = site_url() . 'signin?usid=' . urlencode(base64_encode($verificationCode));
			$userName = $this->mail->generateUnique('users', strstr($email, '@', true), 'user_name', NULL, NULL);
            //$userName = $this->mail->generateUnique('users', trim($firstName . $lastName), 'user_name', NULL, NULL);
			$query = $this->db->get('users');
			if ($query->num_rows() > 0) {
				$role = 'user';
			}else{
				$role = 'administrator';
			}
            $this->auth->setUserName($userName);
            $this->auth->setFirstName(trim($firstName));
            $this->auth->setLastName(trim($lastName));
            $this->auth->setEmail($email);
            $this->auth->setPassword($password);
            $this->auth->setContactNo($contactNo);
            $this->auth->setAddress($address);
            $this->auth->setCompanyName($companyName);
			$this->auth->setCountry($country);
			$this->auth->setProvince($province);
            $this->auth->setVerificationCode($verificationCode);
            $this->auth->setTimeStamp($timeStamp);
            $this->auth->setStatus($status);
			$this->auth->setRole($role);
            $chk = $this->auth->create();
			$result = $this->auth->getUserID();
			if (!empty($result) && count($result) > 0) {
                foreach ($result as $row) {
                    $authArray = array(
                        'user_id' => $row->user_id,
                    );
				}
			}
			if ($role != "administrator") {
				$datetime = date('Y-m-d H:i:s');
				$this->pay->setUserID($row->user_id);
				$this->pay->setTokens(5);
				$this->pay->setLastdate($datetime);
				$ft = $this->pay->freeTokens();
			}
            if ($chk === TRUE) {
                $this->load->library('encrypt');
                $mailData = array('topMsg' => 'Hi', 'bodyMsg' => 'Congratulations, Your registration has been successfully submitted.', 'thanksMsg' => SITE_DELIMETER_MSG, 'delimeter' => SITE_DELIMETER, 'verificationLink' => $verificationLink);
                $this->mail->setMailTo($email);
                $this->mail->setMailFrom('shaun@hi5digital.co.za', 'Rola Motors');
                $this->mail->setMailSubject('User Registeration!');
                $this->mail->setMailContent($mailData);
                $this->mail->setTemplateName('verification');
                $this->mail->setTemplatePath('mailTemplate/');
                $chkStatus = $this->mail->sendMail(MAILING_SERVICE_PROVIDER);
                if ($chkStatus === TRUE) {
                    redirect('signin');
                } else {
                    echo 'Error';
                }
            } else {
                
            }
        }
    } 
    public function editUser() {
        $this->form_validation->set_rules('first_name', 'First Name', 'required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required');
        $this->form_validation->set_rules('contact_no', 'Contact No', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('company_name', 'Company Name', 'required');
		$this->form_validation->set_rules('country', 'Country', 'required');
		$this->form_validation->set_rules('province', 'Province', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->edit();
        } else {
            $firstName = $this->input->post('first_name');
            $lastName = $this->input->post('last_name');
            $contactNo = $this->input->post('contact_no');
            $companyName = $this->input->post('company_name');
            $address = $this->input->post('address');
			$country = $this->input->post('country');
			$province = $this->input->post('province');
            $timeStamp = time();
            $sessionArray = $this->session->userdata('ci_seesion_key');
            $this->auth->setUserID($sessionArray['user_id']);
            $this->auth->setFirstName(trim($firstName));
            $this->auth->setLastName(trim($lastName));
            $this->auth->setContactNo($contactNo);
            $this->auth->setAddress($address);
            $this->auth->setCompanyName($companyName);
			$this->auth->setCountry($country);
			$this->auth->setProvince($province);
            $this->auth->setTimeStamp($timeStamp);
            $status = $this->auth->update();
            if ($status == TRUE) {
                redirect('profile');
            }
        }
    }
    function doLogin() {
		$this->load->library('session');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('user_name', 'User Name/Email', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->login();
        } else {
            $sessArray = array();
            $username = $this->input->post('user_name');
            $password = $this->input->post('password');
            $this->auth->setUserName($username);
            $this->auth->setPassword($password);
            $result = $this->auth->login();
            if (!empty($result) && count($result) > 0) {
                foreach ($result as $row) {
                    $authArray = array(
                        'user_id' => $row->user_id,
                        'user_name' => $row->user_name,
                        'email' => $row->email,
						'role' => $row->role
                    );
                    $this->session->set_userdata('ci_session_key_generate', TRUE);
                    $this->session->set_userdata('ci_seesion_key', $authArray);
					$this->session->set_userdata('role',$row->role);
					$this->session->set_userdata('user_id',$row->user_id);
					$this->session->set_userdata('user_name',$row->user_name);
					$this->session->set_userdata('email',$row->email);
                }
                redirect('profile');
            } else {
                redirect('signin?msg=1');
            }
        }
    }
    public function actionChangePwd() {
        $this->form_validation->set_rules('change_pwd_password', 'Password', 'trim|required|min_length[8]');
        $this->form_validation->set_rules('change_pwd_confirm_password', 'Password Confirmation', 'trim|required|matches[change_pwd_password]');
        if ($this->form_validation->run() == FALSE) {
            $this->changepwd();
        } else {
            $change_pwd_password = $this->input->post('change_pwd_password');
            $sessionArray = $this->session->userdata('ci_seesion_key');
            $this->auth->setUserID($sessionArray['user_id']);
            $this->auth->setPassword($change_pwd_password);
            $status = $this->auth->changePassword();
            if ($status == TRUE) {
                redirect('profile');
            }
        }
    }
    public function actionForgotPassword() {
        $this->form_validation->set_rules('forgot_email', 'Your Email', 'trim|required|valid_email');
        if ($this->form_validation->run() == FALSE) {
            $this->forgotpassword();
        } else {
            $login = site_url() . 'signin';
            $email = $this->input->post('forgot_email');
            $this->auth->setEmail($email);
            $pass = $this->generateRandomPassword(8);
            $this->auth->setPassword($pass);
            $status = $this->auth->updateForgotPassword();
            if ($status == TRUE) {
                $this->load->library('encrypt');
                $mailData = array('topMsg' => 'Hi', 'bodyMsg' => 'Your password has been reset successfully!.', 'thanksMsg' => SITE_DELIMETER_MSG, 'delimeter' => SITE_DELIMETER, 'loginLink' => $login, 'pwd' => $pass, 'username' => $email);
                $this->mail->setMailTo($email);
                $this->mail->setMailFrom(MAIL_FROM);
                $this->mail->setMailSubject('Forgot Password!');
                $this->mail->setMailContent($mailData);
                $this->mail->setTemplateName('sendpwd');
                $this->mail->setTemplatePath('mailTemplate/');
                $chkStatus = $this->mail->sendMail(MAILING_SERVICE_PROVIDER);
                if ($chkStatus === TRUE) {
                    redirect('forgotpwd?msg=2');
                } else {
                    redirect('forgotpwd?msg=1');
                }
            } else {
                redirect('forgotpwd?msg=1');
            }
        }
    }
    public function generateRandomPassword($length = 10) {
        $alphabets = range('a', 'z');
        $numbers = range('0', '9');
        $final_array = array_merge($alphabets, $numbers);
        $password = '';
        while ($length--) {
            $key = array_rand($final_array);
            $password .= $final_array[$key];
        }
        return $password;
    }
    public function logout() {
        $this->session->unset_userdata('ci_seesion_key');
        $this->session->unset_userdata('ci_session_key_generate');
        $this->session->sess_destroy();
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
        redirect('signin');
    }
}