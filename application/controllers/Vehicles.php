<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Vehicles extends CI_Controller {
	public function __construct(){
        parent::__construct();
        $this->load->model('Vehicles_model', 'vehicles');
		$this->load->model('Pay_model', 'pay');
		$this->load->model('Users_model', 'users');
		$this->load->model('Mail', 'mail');
        $this->load->helper('url_helper');
        $this->load->helper('form');
		$this->load->helper('url');
        $this->load->library('form_validation');
		$this->load->library('session');
		if ($this->session->userdata('ci_session_key_generate') == FALSE) { 
            redirect('signin');
        }
		/*if ($this->session->userdata('role') != "administrator") {
			 redirect('profile');
		}*/
    }
    public function index(){
		$data = array();
        $data['vehicles'] = $this->vehicles->vehicles_list();
		$this->load->view('templates/header', $data);
		$this->load->view('vehicles/index');
		$this->load->view('templates/footer', $data);
    }
	public function register() {
		$sessionArray = $this->session->userdata('ci_seesion_key');
		$uid = $sessionArray['user_id'];
		$this->pay->setUserID($uid);
		$data['tokenData'] = $this->pay->getTokenDetails();
		$no_of_avl_token = $data['tokenData']['tokens'];
		if($no_of_avl_token == 0 && $this->session->userdata('role') != "administrator"){
			$this->load->view('templates/header');
			$this->load->view('vehicles/no_token_available');
			$this->load->view('templates/footer');
		}else{
			$this->load->view('templates/header');
			$this->load->view('vehicles/register');
			$this->load->view('templates/footer');
		}
    }
	public function actionCreate() {
		$files = $_FILES;
        $this->form_validation->set_rules('make', 'Make', 'required');
        $this->form_validation->set_rules('model', 'Model', 'required');
        $this->form_validation->set_rules('year', 'Year', 'required');
        $this->form_validation->set_rules('mmcode', 'M&M Code', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->register();
        } else {
			$data = array();
            $make = $this->input->post('make');
            $model = $this->input->post('model');
            $year = $this->input->post('year');
            $mmCode = $this->input->post('mmcode');
			$sessionArray = $this->session->userdata('ci_seesion_key');
			$uid = $sessionArray['user_id'];
			$this->pay->setUserID($uid);
			$data['tokenData'] = $this->pay->getTokenDetails();
			$no_of_available_token = $data['tokenData']['tokens'];
			$this->session->set_userdata('no_of_available_token', $no_of_available_token);
       		$this->session->set_userdata('vmake', $make);
			$this->session->set_userdata('vmodel', $model);
			$this->session->set_userdata('vyear', $year);
			$this->session->set_userdata('vmmcode', $mmCode);
			redirect('vehicles/vhcimguld');
        }
    }
	public function vhcimguld() {
		$this->load->view('templates/header');
		$this->load->view('vehicles/vehicle_image_upload');
		$this->load->view('templates/footer');
    }
	public function vimguld() {
		$files = $_FILES;
		$data = array();
		$this->form_validation->set_rules('vehiclebgimage', 'Vehicle Background Image', 'required');
        if(isset($_FILES["vehicleimage"]) && $_FILES["vehicleimage"]["error"][0] == 4) {
			$this->form_validation->set_rules('vehicleimage', 'Vehicle Image', 'required');
		}
        if ($this->form_validation->run() == FALSE) {
            $this->vhcimguld();
        } else {
			$vimgs = $_FILES['vehicleimage'];
            $make = $this->input->post('make');
            $model = $this->input->post('model');
            $year = $this->input->post('year');
            $mmCode = $this->input->post('mmcode');
			$sessionArray = $this->session->userdata('ci_seesion_key');
       		$this->vehicles->setUserID($sessionArray['user_id']);
            $this->vehicles->setMake($make);
            $this->vehicles->setModel($model);
            $this->vehicles->setYear($year);
            $this->vehicles->setMMCode($mmCode);
			$vehiclebgimage = $this->input->post('vehiclebgimage');
			$this->vehicles->setVehicleBGImage($vehiclebgimage);
			$count = count($_FILES['vehicleimage']['name']);
			for($i=0; $i<$count; $i++){
				$_FILES['vehicleimage']['name']= time().$files['vehicleimage']['name'][$i];
				$_FILES['vehicleimage']['type']= $files['vehicleimage']['type'][$i];
				$_FILES['vehicleimage']['tmp_name']= $files['vehicleimage']['tmp_name'][$i];
				$_FILES['vehicleimage']['error']= $files['vehicleimage']['error'][$i];
				$_FILES['vehicleimage']['size']= $files['vehicleimage']['size'][$i];
				$config['upload_path'] = './assets/vimages/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				//$config['max_size'] = '2048000';
				//$config['remove_spaces'] = true;
				//$config['overwrite'] = false;
				//$config['max_width'] = '1920';
				//$config['max_height'] = '1080';
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				$this->upload->do_upload('vehicleimage');
				$fileName = str_replace(' ', '_', $_FILES['vehicleimage']['name']);
				$images[] = $fileName;
			}
			$noOfImages = count($images);
			$vehicleImages = implode(',',$images);
			$this->vehicles->setVehicleImages($vehicleImages);
			$this->vehicles->setNoOfImages($noOfImages);
			$uploadedDate = date('d-m-Y');
			$this->vehicles->setUploadedDate($uploadedDate);
			$imagesZipPath = '-';
			$imagesZipName = '-';
			$this->vehicles->setImagesZipPath($imagesZipPath);
			$this->vehicles->setImagesZipName($imagesZipName);
			$vid = $this->vehicles->create();
			$this->vehicles->setVehicleID($vid);
            $data['vehicleInfo'] = $this->vehicles->getVehicleDetails();
			$vehicleInfoData = array(
                'vehicle_images' => $data['vehicleInfo']['vehicle_images'],
                'id' => $data['vehicleInfo']['id'],
                ); 
			$this->session->set_userdata('vehicleInfo', $vehicleInfoData);
			redirect('vehicles/vhcdt');
        }
    }
	public function vhcdt() {
		$this->load->view('templates/header');
		$this->load->view('vehicles/vehicle_data');
		$this->load->view('templates/footer');
    }
	public function vimguldvftn() {
		$files = $_FILES;
		$vimgs = $_FILES['vehicleimage'];
		$sessionArray = $this->session->userdata('ci_seesion_key');
		$this->vehicles->setUserID($sessionArray['user_id']);
		$make = $this->input->post('make');
		$model = $this->input->post('model');
		$year = $this->input->post('year');
		$mmCode = $this->input->post('mmcode');
		$this->vehicles->setMake($make);
		$this->vehicles->setModel($model);
		$this->vehicles->setYear($year);
		$this->vehicles->setMMCode($mmCode);
		if($_FILES["vehicleimage"]["error"][0] != 4) {
			$count = count($_FILES['vehicleimage']['name']);
			for($i=0; $i<$count; $i++){
				$_FILES['vehicleimage']['name']= time().$files['vehicleimage']['name'][$i];
				$_FILES['vehicleimage']['type']= $files['vehicleimage']['type'][$i];
				$_FILES['vehicleimage']['tmp_name']= $files['vehicleimage']['tmp_name'][$i];
				$_FILES['vehicleimage']['error']= $files['vehicleimage']['error'][$i];
				$_FILES['vehicleimage']['size']= $files['vehicleimage']['size'][$i];
				$config['upload_path'] = './assets/vimages/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				//$config['max_size'] = '2048000';
				//$config['remove_spaces'] = true;
				//$config['overwrite'] = false;
				//$config['max_width'] = '1920';
				//$config['max_height'] = '1080';
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				$this->upload->do_upload('vehicleimage');
				$fileName = str_replace(' ', '_', $_FILES['vehicleimage']['name']);
				$images[] = $fileName;
			}
			$vid = $this->input->post('id');
			$this->vehicles->setVehicleID($vid);
			$data['vehicleInfo'] = $this->vehicles->getVehicleDetails();
			$bf_vimags = explode(',',$data['vehicleInfo']['vehicle_images']);
			$vimags = array_filter($bf_vimags);
			$u_imgs = array_merge($vimags,$images);
			$noOfImages = count($u_imgs);
			$vehicleImages = implode(',',$u_imgs);
			$this->vehicles->setVehicleImages($vehicleImages);
			$this->vehicles->setNoOfImages($noOfImages);
			$uploadedDate = date('d-m-Y');
			$this->vehicles->setUploadedDate($uploadedDate);
		}else{
			$vid = $this->input->post('id');
			$this->vehicles->setVehicleID($vid);
			$data['vehicleInfo'] = $this->vehicles->getVehicleDetails();
			$bf_vimags = explode(',',$data['vehicleInfo']['vehicle_images']);
			$vimags = array_filter($bf_vimags);
			$noOfImages = count($vimags);
			$vehicleImages = $data['vehicleInfo']['vehicle_images'];
			$this->vehicles->setVehicleImages($vehicleImages);
			$this->vehicles->setNoOfImages($noOfImages);
			$uploadedDate = date('d-m-Y');
			$this->vehicles->setUploadedDate($uploadedDate);
		}
		$imagesZipPath = '-';
		$imagesZipName = '-';
		$this->vehicles->setImagesZipPath($imagesZipPath);
		$this->vehicles->setImagesZipName($imagesZipName);
		$chk = $this->vehicles->update();
		$vehicleInfoData = array(
			'vehicle_images' => $vehicleImages,
			'id' => $vid,
			); 
		$this->session->set_userdata('vehicleInfo', $vehicleInfoData);
		redirect('vehicles/vhlvrf');
    }
	public function vhlvrf() {
		$this->load->view('templates/header');
		$this->load->view('vehicles/vehicle_image_verify');
		$this->load->view('templates/footer');
    }
	public function vimguldpc() {
		$data = array();
		$vid = $this->input->post('id');
		$this->vehicles->setVehicleID($vid);
		$data['vehicleInfo'] = $this->vehicles->getVehicleDetails();
		if($data['vehicleInfo']['images_zip_path'] == "-"){
			$this->load->library('zip');
			$vimags = explode(',',$data['vehicleInfo']['vehicle_images']);
			$no_of_tokens = count($vimags);
			$sessionArray = $this->session->userdata('ci_seesion_key');
			$uid = $sessionArray['user_id'];
			$this->pay->setUserID($uid);
			$role = $this->session->userdata('role');
			if($role != "administrator"){
				$data['tokenData'] = $this->pay->getTokenDetails();
				$token_id = $data['tokenData']['id'];
				$no_of_updated_token = $data['tokenData']['tokens'] - $no_of_tokens;
				$date_time = date('Y-m-d H:i:s');
				$this->pay->setTokenID($token_id);
				$this->pay->setNoOfUpdatedToken($no_of_updated_token);
				$this->pay->setLastdate($date_time);
			}
			foreach($vimags as $vimag):
				$client = new GuzzleHttp\Client();
				$res = $client->post('https://api.remove.bg/v1.0/removebg', [
					'multipart' => [
						[
							'name'     => 'image_file',
							'contents' => fopen(FCPATH .'assets/vimages/'.$vimag, 'r')
						],
						[
							'name'     => 'bg_image_file',
							'contents' => fopen(FCPATH ."assets/bgimages/".$data['vehicleInfo']['vehicle_bgimage'], 'r')
						],
						[
							'name'     => 'size',
							'contents' => 'auto'
						]
					],
					'headers' => [
						'X-Api-Key' => 'GPfrhtNiVudgC4tYLzy2nnkj'
					]
				]);
				$fp = fopen(FCPATH ."assets/vimages/".$vimag, "wb");
				fwrite($fp, $res->getBody());
				fclose($fp);
				$full_path_images[] = FCPATH.'/assets/vimages/'.$vimag;
			endforeach;
			$filename = time()."_vehicle_images.zip";
			$imagesZipPath = FCPATH.'/assets/images_zip/'.$filename;
			foreach($full_path_images as $vimag):
				$this->zip->read_file($vimag);
			endforeach;
			$this->zip->archive($imagesZipPath);
			$imagesZipPathDB = base_url('assets/images_zip/'.$filename);
			$this->vehicles->setImagesZipPath($imagesZipPathDB);
			$this->vehicles->setImagesZipName($filename);
			$chk = $this->vehicles->updateImagesZipPath();
			if($role != "administrator"){
				$ut = $this->pay->updateTokens();
			}
		}
		redirect('vehicles/vhlimgs');
	}
	public function vhlimgs() {
		$this->load->view('templates/header');
		$this->load->view('vehicles/vehicle_processed');
		$this->load->view('templates/footer');
    }
	public function vhllist($vid = NULL){
		$data = array();
		$sessionArray = $this->session->userdata('ci_seesion_key');
		//$vid = $this->uri->segment(3);
		if (!empty($vid)) {
			$this->vehicles->setUserID($vid);
		} else {
			$this->vehicles->setUserID($sessionArray['user_id']);
		}
        $data['vehicles'] = $this->vehicles->vhllist();
		$this->load->view('templates/header', $data);
		$this->load->view('vehicles/list');
		$this->load->view('templates/footer', $data);
    }
	public function view($vid = NULL) {
		//$vid = $this->uri->segment(3);
		if (empty($vid)) {
			redirect('vehicles/list');
		} else {
			$data = array();
			$this->vehicles->setVehicleID($vid);
			$data['vehicleInfo'] = $this->vehicles->getVehicleDetails();
			if($data['vehicleInfo']['images_zip_path'] == "-"){
				$this->load->library('zip');
				$vimags = explode(',',$data['vehicleInfo']['vehicle_images']);
				foreach($vimags as $vimag):
					$full_path_images[] = FCPATH.'/assets/vimages/'.$vimag;
				endforeach;
				$filename = time()."_vehicle_images.zip";
				$imagesZipPath = FCPATH.'/assets/images_zip/'.$filename;
				foreach($full_path_images as $vimag):
					$this->zip->read_file($vimag);
				endforeach;
				$this->zip->archive($imagesZipPath);
				$imagesZipPathDB = base_url('assets/images_zip/'.$filename);
				$this->vehicles->setImagesZipPath($imagesZipPathDB);
				$this->vehicles->setImagesZipName($filename);
				$chk = $this->vehicles->updateImagesZipPath();
			}
			$this->load->view('templates/header');
			$this->load->view('vehicles/view', $data);
			$this->load->view('templates/footer');
		}
	}
	public function download($vid = NULL) {
		//$vid = $this->uri->segment(3);
		if (empty($vid)) {
			redirect('vehicles/view/'.$vid);
		} else {
			$this->load->library('zip');
			$this->vehicles->setVehicleID($vid);
			$data['vehicleInfo'] = $this->vehicles->getVehicleDetails();
			$vimags = explode(',',$data['vehicleInfo']['vehicle_images']);
				foreach($vimags as $vimag):
					$full_path_images[] = FCPATH.'/assets/vimages/'.$vimag;
				endforeach;
				$filename = time()."_vehicle_images.zip";
				$imagesZipPath = FCPATH.'/assets/images_zip/'.$filename;
				foreach($full_path_images as $vimag):
					$this->zip->read_file($vimag);
				endforeach;
			$imagesZipName = $data['vehicleInfo']['images_zip_name'];
			$this->zip->download($imagesZipName);
			$this->load->view('templates/header');
			$this->load->view('vehicles/view/'.$vid);
			$this->load->view('templates/footer');
		}
	}
	public function sendmail($vid = NULL) {
		//$vid = $this->uri->segment(3);
		if (empty($vid)) {
			redirect('vehicles/view/'.$vid);
		} else {
			$this->load->library('zip');
			$this->vehicles->setVehicleID($vid);
			$data['vehicleInfo'] = $this->vehicles->getVehicleDetails();
			$uid = $data['vehicleInfo']['u_id'];
			$this->vehicles->setUserID($uid);
            $data['userInfo'] = $this->vehicles->getUserDetails();
			$imagesZipPathForMail = $data['vehicleInfo']['images_zip_path'];
			$u_email = $data['userInfo']['email'];
			$this->load->library('encrypt');
			$btn = '<p><a href="'.base_url('vehicles/download/'.$data['vehicleInfo']['id']).'" style="background-color:#c00;color:#fff;padding:10px 30px;font-size:14px;text-decoration: none;text-transform: uppercase;position: relative;display: inline-block;margin-top: 15px;font-weight: 600;" target="_blank">Click Here</a></p>';
			$mailData = array('topMsg' => 'Hi', 'bodyMsg' => "Please download Vehicle Images Zip File using below click button.".$btn, 'thanksMsg' => SITE_DELIMETER_MSG, 'delimeter' => SITE_DELIMETER);
			$this->mail->setMailTo($u_email);
			$this->mail->setMailFrom('shaun@hi5digital.co.za', 'Rola Motors');
			$this->mail->setMailSubject('Vehicle Images!');
			$this->mail->setMailContent($mailData);
			$this->mail->setTemplateName('imageszippath');
			$this->mail->setTemplatePath('mailTemplate/');
			$this->mail->setImagesZipPath($imagesZipPathForMail);
			$chkStatus = $this->mail->sendMail($data);
			if ($chkStatus === TRUE) {
				redirect('vehicles/view/'.$vid);
			}
		}
	}
	public function edit($vid = NULL){
		//$vid = $this->uri->segment(3);
		if (empty($vid)) {
            redirect('vehicles/vhllist');
        } else {
            $data = array();
			$this->vehicles->setVehicleID($vid);
            $data['vehicleInfo'] = $this->vehicles->getVehicleDetails();
			$this->load->view('templates/header', $data);
			$this->load->view('vehicles/edit');
			$this->load->view('templates/footer', $data);
        }
    }
	public function deleteVehicleImg(){
		$data = array();
		$didata = array(
            'id' => $this->input->post('id'),
            'title' => $this->input->post('title')
        );
		$d_img[] = $this->input->post('title');
		$vid = $this->input->post('id');
		$vtitle = $this->input->post('title');
		$this->vehicles->setVehicleID($vid);
        $data['vehicleInfo'] = $this->vehicles->getVehicleDetails();
		$vimags = explode(',',$data['vehicleInfo']['vehicle_images']);
		foreach($vimags as $vimag):
			$v_imgs[] = $vimag;
		endforeach;
		$u_imgs = array_diff($v_imgs, $d_img);
		@unlink('assets/vimages/'.$vtitle);
		$noOfImages = count($u_imgs);
		$vehicleImages = implode(',',$u_imgs);
		$this->vehicles->setVehicleImages($vehicleImages);
		$this->vehicles->setNoOfImages($noOfImages);
		$chk = $this->vehicles->updateImages();
		$vehicleInfoData = array(
			'vehicle_images' => $vehicleImages,
			'id' => $vid,
			); 
		$this->session->set_userdata('vehicleInfo', $vehicleInfoData);
		if ($chk === TRUE) {
			$status = 'ok'; 
		}
		echo $status; die;
    }
	public function editVehicle() {
		$files = $_FILES;
        $this->form_validation->set_rules('make', 'Make', 'required');
        $this->form_validation->set_rules('model', 'Model', 'required');
        $this->form_validation->set_rules('year', 'Year', 'required');
        $this->form_validation->set_rules('mmcode', 'M&M Code', 'required');
		/*if(empty($_FILES['vehicleimage']['name'])){
			$this->form_validation->set_rules('vehicleimage', 'Vehicle Images', 'trim|xss_clean');
		}*/
        if ($this->form_validation->run() == FALSE) {
            $this->register();
        } else {
			$vimgs = $_FILES['vehicleimage'];
            $make = $this->input->post('make');
            $model = $this->input->post('model');
            $year = $this->input->post('year');
            $mmCode = $this->input->post('mmcode');
			$sessionArray = $this->session->userdata('ci_seesion_key');
       		$this->vehicles->setUserID($sessionArray['user_id']);
            $this->vehicles->setMake($make);
            $this->vehicles->setModel($model);
            $this->vehicles->setYear($year);
            $this->vehicles->setMMCode($mmCode);
			if($_FILES["vehicleimage"]["error"][0] != 4) {
				$count = count($_FILES['vehicleimage']['name']);
				for($i=0; $i<$count; $i++){
					$_FILES['vehicleimage']['name']= time().$files['vehicleimage']['name'][$i];
					$_FILES['vehicleimage']['type']= $files['vehicleimage']['type'][$i];
					$_FILES['vehicleimage']['tmp_name']= $files['vehicleimage']['tmp_name'][$i];
					$_FILES['vehicleimage']['error']= $files['vehicleimage']['error'][$i];
					$_FILES['vehicleimage']['size']= $files['vehicleimage']['size'][$i];
					$config['upload_path'] = './assets/vimages/';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$config['max_size'] = '2048000';
					//$config['remove_spaces'] = true;
					//$config['overwrite'] = false;
					//$config['max_width'] = '1920';
					//$config['max_height'] = '1080';
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					$this->upload->do_upload('vehicleimage');
					$fileName = str_replace(' ', '_', $_FILES['vehicleimage']['name']);
					$client = new GuzzleHttp\Client();
					$res = $client->post('https://api.remove.bg/v1.0/removebg', [
						'multipart' => [
							[
								'name'     => 'image_file',
								'contents' => fopen(FCPATH .'assets/vimages/'.$fileName, 'r')
							],
							[
								'name'     => 'bg_image_file',
								'contents' => fopen(FCPATH .'assets/vimages/no-bg.jpg', 'r')
							],
							[
								'name'     => 'size',
								'contents' => 'auto'
							]
						],
						'headers' => [
							'X-Api-Key' => 'hjD4fCSkcvqmU5hBoXptn4rb'
						]
					]);
					$fp = fopen(FCPATH ."assets/vimages/".$fileName, "wb");
					fwrite($fp, $res->getBody());
					fclose($fp);
					$images[] = $fileName;
				}
				$vid = $this->input->post('id');
				$this->vehicles->setVehicleID($vid);
				$data['vehicleInfo'] = $this->vehicles->getVehicleDetails();
				$bf_vimags = explode(',',$data['vehicleInfo']['vehicle_images']);
				$vimags = array_filter($bf_vimags);
				$u_imgs = array_merge($vimags,$images);
				$noOfImages = count($u_imgs);
				$vehicleImages = implode(',',$u_imgs);
				$this->vehicles->setVehicleImages($vehicleImages);
				$this->vehicles->setNoOfImages($noOfImages);
				$uploadedDate = date('d-m-Y');
				$this->vehicles->setUploadedDate($uploadedDate);
				$images_zip_name = $data['vehicleInfo']['images_zip_name'];
				@unlink('assets/images_zip/'.$images_zip_name);
				$this->load->library('zip');
				$vimags = explode(',',$data['vehicleInfo']['vehicle_images']);
				foreach($u_imgs as $u_img):
					$full_path_images[] = FCPATH.'/assets/vimages/'.$u_img;
				endforeach;
				$filename = time()."_vehicle_images.zip";
				$imagesZipPath = FCPATH.'/assets/images_zip/'.$filename;
				foreach($full_path_images as $vimag):
					$this->zip->read_file($vimag);
				endforeach;
				$this->zip->archive($imagesZipPath);
				$imagesZipPathDB = base_url('assets/images_zip/'.$filename);
				$this->vehicles->setImagesZipPath($imagesZipPathDB);
				$this->vehicles->setImagesZipName($filename);
			}
			$chk = $this->vehicles->update();
            redirect('vehicles/vhllist');
        }
    }
	public function deleteVehicle(){
		$data = array();
		$didata = array(
            'id' => $this->input->post('id'),
        );
		$vid = $this->input->post('id');
		$this->vehicles->setVehicleID($vid);
        $data['vehicleInfo'] = $this->vehicles->getVehicleDetails();
		$vimags = explode(',',$data['vehicleInfo']['vehicle_images']);
		foreach($vimags as $vimag):
			@unlink('assets/vimages/'.$vimag);
		endforeach;
		$images_zip_name = $data['vehicleInfo']['images_zip_name'];
		@unlink('assets/images_zip/'.$images_zip_name);
		$chk = $this->vehicles->deleteVehicle();
		if ($chk === TRUE) {
			$status = 'ok'; 
		}
		echo $status; die;
    }
}