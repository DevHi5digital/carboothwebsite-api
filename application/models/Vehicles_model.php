<?php
class Vehicles_model extends CI_Model {
	private $_vehicleID;
	private $_userID;
	private $_make;
    private $_model;
    private $_year;
    private $_mmcode;
	private $_noOfImages;
	private $_uploadedDate;
	private $_vehicleImages;
	private $_vImages;
	private $_imagesZipPath;
	private $_imagesZipName;
	private $vehicleBGImage;
	public function setVehicleBGImage($vehicleBGImage) {
        $this->_vehicleBGImage = $vehicleBGImage;
    }
	public function setVehicleID($vehicleID) {
        $this->_vehicleID = $vehicleID;
    }
	public function setUserID($userID) {
        $this->_userID = $userID;
    }
    public function setMake($make) {
        $this->_make = $make;
    }
    public function setModel($model) {
        $this->_model = $model;
    }
    public function setYear($year) {
        $this->_year = $year;
    }
    public function setMMCode($mmcode) {
        $this->_mmcode = $mmcode;
    }
	public function setVehicleImages($vehicleImages) {
        $this->_vehicleImages = $vehicleImages;
    }
	public function setVImages($vImages) {
        $this->_vImages = $vImages;
    }
	public function setNoOfImages($noOfImages) {
        $this->_noOfImages = $noOfImages;
    }
	public function setUploadedDate($uploadedDate) {
        $this->_uploadedDate = $uploadedDate;
    }
	public function setImagesZipPath($imagesZipPath) {
        $this->_imagesZipPath = $imagesZipPath;
    }
	public function setImagesZipName($imagesZipName) {
        $this->_imagesZipName = $imagesZipName;
    }
    public function vehicles_list(){
        $query = $this->db->get('vehicles');
        return $query->result();
    }
	public function create($userData = array()) {
         $data = array(
			'u_id' => $this->_userID,
            'make' => $this->_make,
            'model' => $this->_model,
            'year' => $this->_year,
            'mmcode' => $this->_mmcode,
			'vehicle_images' => $this->_vehicleImages,
			'no_of_images' => $this->_noOfImages,
			'uploaded_date' => $this->_uploadedDate,	
			'images_zip_path' => $this->_imagesZipPath,
			'images_zip_name' => $this->_imagesZipName,
			'vehicle_bgimage' => $this->_vehicleBGImage,
        );
        $this->db->insert('vehicles', $data);
        if (!empty($this->db->insert_id()) && $this->db->insert_id() > 0) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }
	public function vhllist(){
		$this->db->where('u_id', $this->_userID);
        $query = $this->db->get('vehicles');
        return $query->result();
    }
	public function getVehicleDetails() {
        $this->db->select(array('m.id as id', 'm.u_id', 'm.make', 'm.model', 'm.year', 'm.mmcode', 'm.vehicle_images', 'm.no_of_images', 'm.images_zip_path','m.images_zip_name', 'm.uploaded_date', 'm.vehicle_bgimage'));
        $this->db->from('vehicles as m');
        $this->db->where('m.id', $this->_vehicleID);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return FALSE;
        }
    }
	public function getUserDetails() {
        $this->db->select(array('m.id as user_id', 'CONCAT(m.first_name, " ", m.last_name) as full_name', 'm.first_name', 'm.last_name', 'm.email', 'm.contact_no', 'm.address', 'm.company_name', 'm.country', 'm.province', 'm.created_date', 'm.status', 'm.role'));
        $this->db->from('users as m');
        $this->db->where('m.id', $this->_userID);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return FALSE;
        }
    }
	public function updateImagesZipPath() {
        $data = array(
            'images_zip_path' => $this->_imagesZipPath,
			'images_zip_name' => $this->_imagesZipName,
        );
        $this->db->where('id', $this->_vehicleID);
        $msg = $this->db->update('vehicles', $data);
        if ($msg == 1) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
	public function updateImages() {
        $data = array(
            'vehicle_images' => $this->_vehicleImages,
			'no_of_images' => $this->_noOfImages,
        );
        $this->db->where('id', $this->_vehicleID);
        $msg = $this->db->update('vehicles', $data);
        if ($msg == 1) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
	public function update() {
        $data = array(
            'u_id' => $this->_userID,
            'make' => $this->_make,
            'model' => $this->_model,
            'year' => $this->_year,
            'mmcode' => $this->_mmcode,
			'vehicle_images' => $this->_vehicleImages,
			'no_of_images' => $this->_noOfImages,
			'uploaded_date' => $this->_uploadedDate,	
			'images_zip_path' => $this->_imagesZipPath,
			'images_zip_name' => $this->_imagesZipName,
        );
        $this->db->where('id', $this->_vehicleID);
        $msg = $this->db->update('vehicles', $data);
        if ($msg == 1) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
	public function deleteVehicle(){
		$this->db->where('id', $this->_vehicleID);
		$msg = $this->db->delete('vehicles');
		if ($msg == 1) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}