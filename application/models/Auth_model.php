<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Auth_model extends CI_Model {
    private $_userID;
    private $_userName;
    private $_firstName;
    private $_lastName;
    private $_email;
    private $_password;
    private $_contactNo;
    private $_address;
    private $_companyName;
    private $_verificationCode;
    private $_timeStamp;
    private $_status;
	private $_role;
	private $_country;
	private $_province;
    public function setUserID($userID) {
        $this->_userID = $userID;
    }
    public function setUserName($userName) {
        $this->_userName = $userName;
    }
    public function setFirstname($firstName) {
        $this->_firstName = $firstName;
    }
    public function setLastName($lastName) {
        $this->_lastName = $lastName;
    }
    public function setEmail($email) {
        $this->_email = $email;
    }
    public function setContactNo($contactNo) {
        $this->_contactNo = $contactNo;
    }
    public function setPassword($password) {
        $this->_password = $password;
    }
    public function setAddress($address) {
        $this->_address = $address;
    }
    public function setCompanyName($companyName) {
        $this->_companyName = $companyName;
    }
    public function setVerificationCode($verificationCode) {
        $this->_verificationCode = $verificationCode;
    }
    public function setTimeStamp($timeStamp) {
        $this->_timeStamp = $timeStamp;
    }
    public function setStatus($status) {
        $this->_status = $status;
    }
	public function setRole($role) {
        $this->_role = $role;
    }
	public function setCountry($country) {
        $this->_country = $country;
    }
	public function setProvince($province) {
        $this->_province = $province;
    }
    public function create() {
        $hash = $this->hash($this->_password);
        $data = array(
            'user_name' => $this->_userName,
            'first_name' => $this->_firstName,
            'last_name' => $this->_lastName,
            'email' => $this->_email,
            'password' => $hash,
            'contact_no' => $this->_contactNo,
            'address' => $this->_address,
            'company_name' => $this->_companyName,
			'country' => $this->_country,
			'province' => $this->_province,
            'verification_code' => $this->_verificationCode,
            'created_date' => $this->_timeStamp,
            'modified_date' => $this->_timeStamp,
            'status' => $this->_status,
			'role' => $this->_role
        );
        $this->db->insert('users', $data);
        if (!empty($this->db->insert_id()) && $this->db->insert_id() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    function login() {
        $this->db->select('id as user_id, user_name, email, password, role');
        $this->db->from('users');
        $this->db->where('email', $this->_userName);
        $this->db->where('verification_code', 1);
        $this->db->where('status', 1);
        $this->db->or_where('user_name', $this->_userName);
        $this->db->where('verification_code', 1);
        $this->db->where('status', 1);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            $result = $query->result();
            foreach ($result as $row) {
                if ($this->verifyHash($this->_password, $row->password) == TRUE) {
                    return $result;
                } else {
                    return FALSE;
                }
            }
        } else {
            return FALSE;
        }
    }
    public function update() {
        $data = array(
            'first_name' => $this->_firstName,
            'last_name' => $this->_lastName,
            'contact_no' => $this->_contactNo,
            'address' => $this->_address,
            'company_name' => $this->_companyName,
			'country' => $this->_country,
			'province' => $this->_province,
            'modified_date' => $this->_timeStamp,
        );
        $this->db->where('id', $this->_userID);
        $msg = $this->db->update('users', $data);
        if ($msg == 1) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function changePassword() {
        $hash = $this->hash($this->_password);
        $data = array(
            'password' => $hash,
        );
        $this->db->where('id', $this->_userID);
        $msg = $this->db->update('users', $data);
        if ($msg == 1) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function getUserDetails() {
        $this->db->select(array('m.id as user_id', 'CONCAT(m.first_name, " ", m.last_name) as full_name', 'm.first_name', 'm.last_name', 'm.email', 'm.contact_no', 'm.address', 'm.company_name', 'm.country', 'm.province', 'm.created_date', 'm.status', 'm.role'));
        $this->db->from('users as m');
        $this->db->where('m.id', $this->_userID);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return FALSE;
        }
    }
    public function updateForgotPassword() {
        $hash = $this->hash($this->_password);
        $data = array(
            'password' => $hash,
        );
        $this->db->where('email', $this->_email);
        $msg = $this->db->update('users', $data);
        if ($msg > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function activate() {
        $data = array(
            'status' => 1,
            'verification_code' => 1,
        );
        $this->db->where('verification_code', $this->_verificationCode);
        $msg = $this->db->update('users', $data);
        if ($msg == 1) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function hash($password) {
        $hash = password_hash($password, PASSWORD_DEFAULT);
        return $hash;
    }
    public function verifyHash($password, $vpassword) {
        if (password_verify($password, $vpassword)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
	public function getUserID() {
        $this->db->select('id as user_id');
        $this->db->from('users');
        $this->db->where('email', $this->_email);
        $this->db->where('user_name', $this->_userName);
		$this->db->where('first_name', $this->_firstName);
		$this->db->where('last_name', $this->_lastName);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            $result = $query->result();
			return $result;
		}else {
            return FALSE;
        }
    }
}