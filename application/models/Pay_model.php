<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
class Pay_model extends CI_Model{
	private $_userID;
	private $_tokenID;
	private $_tokens;
	private $_lastdate;
	private $_totalAmount;
	private $_noOfTokens;
	private $_dateTime;
	private $_noOfUpdatedToken;
	public function setUserID($userID) {
        $this->_userID = $userID;
    }
	public function setTokenID($tokenID) {
        $this->_tokenID = $tokenID;
    }
    public function setTokens($tokens) {
        $this->_tokens = $tokens;
    }
    public function setLastdate($lastdate) {
        $this->_lastdate = $lastdate;
    }
	public function setTotalAmount($totalAmount) {
        $this->_totalAmount = $totalAmount;
    }
	public function setNoOfTokens($noOfTokens) {
        $this->_noOfTokens = $noOfTokens;
    }
	public function setDateTime($dateTime) {
        $this->_dateTime = $dateTime;
    }
	public function setNoOfUpdatedToken($noOfUpdatedToken) {
        $this->_noOfUpdatedToken = $noOfUpdatedToken;
    }
    public function tokencost_list(){
        $query = $this->db->get('tokencost');
        return $query->result();
    }
	public function freeTokens($tokenData = array()) {
        $data = array(
            'uid' => $this->_userID,
            'tokens' => $this->_tokens,
            'lastdate' => $this->_lastdate,
        );
        $this->db->insert('tokens', $data);
        if (!empty($this->db->insert_id()) && $this->db->insert_id() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
	public function getTokenDetails() {
        $this->db->select(array('tokens', 'lastdate', 'id'));
        $this->db->from('tokens');
        $this->db->where('uid', $this->_userID);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return FALSE;
        }
    }
	public function updateTokens() {
        $data = array(
            'uid' => $this->_userID,
            'tokens' => $this->_noOfUpdatedToken,
            'lastdate' => $this->_lastdate,
        );
        $this->db->where('id', $this->_tokenID);
        $msg = $this->db->update('tokens', $data);
        if ($msg == 1) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
	public function tokensPayments($tokenPaymentData = array()) {
        $data = array(
            'uid' => $this->_userID,
            'amount' => $this->_totalAmount,
            'tokens' => $this->_noOfTokens,
			'datetime' => $this->_dateTime,
        );
        $this->db->insert('payments', $data);
        if (!empty($this->db->insert_id()) && $this->db->insert_id() > 0) {
            $last_insert_id = $this->db->insert_id();
			return  $last_insert_id;
        } else {
            return FALSE;
        }
    }
	public function getPaymentTokenDetails() {
        $this->db->select(array('id', 'amount', 'tokens', 'datetime'));
        $this->db->from('payments');
        $this->db->where('uid', $this->_userID);
		$this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
           return $query->row_array();
        } else {
            return FALSE;
        }
    }
}