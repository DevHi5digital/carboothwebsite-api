<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Mail extends CI_Model {
    private $_mailTo;
    private $_mailFrom;
    private $_mailSubject;
    private $_mailContent;
    private $_templateName;
    private $_templatePath;
	private $_imagesZipPath;
    public function setMailTo($mailTo) {
        $this->_mailTo = $mailTo;
    }
    public function setMailFrom($mailFrom) {
        $this->_mailFrom = $mailFrom;
    }
    public function setMailSubject($mailSubject) {
        $this->_mailSubject = $mailSubject;
    }
    public function setMailContent($mailContent) {
        $this->_mailContent = $mailContent;
    }
    public function setTemplateName($templateName) {
        $this->_templateName = $templateName;
    }
    public function setTemplatePath($templatePath) {
        $this->_templatePath = $templatePath;
    }
	public function setImagesZipPath($imagesZipPath) {
        $this->_imagesZipPath = $imagesZipPath;
    }
    public function sendMail($data) {
        $this->load->library('email');
		$this->load->library('parser');
		$fullPath = $this->_templatePath . $this->_templateName;
		$this->email->clear();
		$config['mailtype'] = "html";
		$this->email->initialize($config);
		$this->email->set_newline("\r\n");
		$mailMessage = $this->load->view($fullPath, $this->_mailContent, TRUE);
		$this->email->to($this->_mailTo);
		$this->email->from('shaun@hi5digital.co.za', 'Rola Motors');
		$this->email->subject($this->_mailSubject);
		$this->email->message($mailMessage);
		//if($this->_imagesZipPath != ''){
			$this->email->attach('http://rentapot.co.za/rola/assets/images_zip/1590493584_vehicle_images.zip');
		//}
		if ($this->email->send()) {
			return TRUE;
		} else {
			return FALSE;
		}
    }
    public function generateUnique($tableName, $string, $field, $key = NULL, $value = NULL) {
        $slug = preg_replace('/[^A-Za-z0-9-]+/', '', strtolower($string));
        $i = 0;
        $params = array();
        $params[$field] = $slug;
        if ($key)
            $params["$key !="] = $value;
        while ($this->db->where($params)->get($tableName)->num_rows()) {
            if (!preg_match('/-{1}[0-9]+$/', $slug))
                $slug .= '-' . ++$i;
            else
                $slug = preg_replace('/[0-9]+$/', ++$i, $slug);
            $params [$field] = $slug;
        }
        return $slug;
    }
}