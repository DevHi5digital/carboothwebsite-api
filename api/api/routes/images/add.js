// MySQL DB Connection
const db = require("../../db");

// Filesystem connection
const fs = require("fs");

var nodemailer = require("nodemailer");
var smtpTransport = require("nodemailer-smtp-transport");
var handlebars = require("handlebars");
const path = require("path");
const request = require("request");

// For decoding the user's id
const jwt = require("jsonwebtoken");

const add_images = (req, res) => {
  // Data is the user-submitted data
  // Files are the provided files
  let data = req.body;
  let photos = req.files["photos"];
  let background = req.files["background"];

  try {
    // 0. Check if the parameters were specified
    if (data.uid == null || photos == null || background == null) {
      return res
        .status(400)
        .send("Missing one or more of the required parameters");
    }

    if (photos.length < 1)
      return res.status(400).send("At least one image must be provided");
    if (photos.length > 35)
      return res.status(400).send("Exceeded 35 image limit");

    if (background.length < 1)
      return res.status(400).send("At least one background must be sent");

    // Set background if it was provided
    background = req.files["background"][0];
    let filePath = path.resolve("./../images/", data.uid);

    // 1. If a UID was specified, check if the relevant directory exists
    if (!fs.existsSync(filePath)) {
      return res.status(404).send("The provided UID doesn't exist");
    }

    // 1.1. Check if there are 35 or more photos in the directory
    if (fs.readdirSync(filePath).length >= 35) {
      return res.status(403).send("Picture upload limit reached (35 pictures)");
    }

    console.log(`Processing ${photos.length} images to ${filePath}`);

    // 2. Process with remove.bg and save the images to the appropriate UID location
    // Old API Key    'X-Api-Key': 'vWT8ncRMLQRe4EFpG8YCQoYk'
    let promise = new Promise(function (resolve, reject) {
      var attachments = [];
      photos.forEach((photo, i) => {
        const settings = {
          url: "https://api.slazzer.com/v2.0/remove_image_background",
          apiKey: "ebe73a1fc9b54c2b92005757a2b612eb",
          sourceImagePath: photo.buffer,
          bgImagePath: background.buffer,
          outputImagePath: filePath,
        };

        console.log("i : " + i);
        request.post(
          {
            url: settings.url,
            formData: {
              source_image_file: fs.createReadStream(settings.sourceImagePath),
              bg_image_file: fs.createReadStream(settings.bgImagePath),
            },
            headers: { "API-KEY": settings.apiKey },
            encoding: null,
          },
          function (error, response, body) {
            if (error) return console.error("Request failed:", error);
            if (response.statusCode != 200)
              return console.error(
                "Error:",
                response.statusCode,
                body.toString("utf8")
              );

            console.log(
              "Photo #" + i + " successfully processed with remove.bg"
            );
            fs.writeFileSync(
              path.resolve(settings.outputImagePath, `${i.toString()}.png`),
              body
            );
            attachments[i] = [];
            attachments[i]["filename"] = `${i.toString()}.png`;
            attachments[i]["path"] = path.resolve(
              filePath,
              `${i.toString()}.png`
            );
            console.log(i);
            console.log(photos.length);
            if (i == photos.length - 1) {
              console.log("resolve");
              resolve(attachments);
            }
          }
        );
      });
    });
    promise
      .then((result) => {
        console.log("-----------result");
        console.log(result);
        db.query(
          `UPDATE tokens SET tokens = tokens - ${photos.length} WHERE uid = ${
            jwt.decode(req.cookies.token).id
          }`
        );
        db.query(
          `SELECT * FROM users left join tokens on tokens.uid=users.id where users.id= ${
            jwt.decode(req.cookies.token).id
          }`,
          [],
          (err, rows) => {
            if (err) throw err;
            readHTMLFile("./emails/pages/email.html", function (err, html) {
              var template = handlebars.compile(html);
              let filePath = path.resolve(
                "./../images/",
                "PyPcoThcNYinVfcdWC7qXR"
              );
              var replacements = {
                username: rows[0]["name"],
                surname: rows[0]["surname"],
                token_Count: rows[0]["tokens"],
                userid: rows[0]["id"],
              };
              var htmlToSend = template(replacements);
              var mailOptions = {
                from: "dev@carbooth.co.za",
                to: rows[0]["email"],
                subject: "Carbooth",
                html: htmlToSend,
                attachments: result,
              };
              smtpTransport.sendMail(mailOptions, function (error, response) {
                if (error) {
                  console.log(error);
                  res.send(error);
                }
              });
            });
          }
        );
        var readHTMLFile = function (path, callback) {
          fs.readFile(path, { encoding: "utf-8" }, function (err, html) {
            if (err) {
              throw err;
              callback(err);
            } else {
              callback(null, html);
            }
          });
        };

        smtpTransport = nodemailer.createTransport({
          host: "smtp.carbooth.co.za",
          secure: false,
          port: 587,
          auth: {
            user: "dev@carbooth.co.za",
            pass: "LetsTryThis2!",
          },
          tls: {
            rejectUnauthorized: false,
          },
        });

        return res
          .status(200)
          .send(`${photos.length} images uploaded successfully`);
      })
      .catch((error) => {
        console.log("Error", error);
      });
  } catch (e) {
    console.log(e);
  }
};

module.exports = add_images;

