// MySQL DB Connection
const db = require('../../db')

// Filesystem connection
const fs = require('fs')
const path = require('path')
const request = require('request');

// For decoding the user's id
const jwt = require('jsonwebtoken')

const add_images = (req, res) => {
    // Data is the user-submitted data
    // Files are the provided files
    let data = req.body
    let photos = req.files['photos']
    let background = req.files['background']

    try{
        // 0. Check if the parameters were specified
        if(data.uid == null || photos == null || background == null){
            return res.status(400).send('Missing one or more of the required parameters')
        }

        if(photos.length < 1) return res.status(400).send('At least one image must be provided')
        if(photos.length > 35) return res.status(400).send('Exceeded 35 image limit')

        if(background.length < 1) return res.status(400).send('At least one background must be sent')

        // Set background if it was provided
        background = req.files['background'][0]
        let filePath = path.resolve('./../images/', data.uid)

        // 1. If a UID was specified, check if the relevant directory exists
        if(!fs.existsSync(filePath)){
            return res.status(404).send('The provided UID doesn\'t exist')
        }

        // 1.1. Check if there are 35 or more photos in the directory
        if(fs.readdirSync(filePath).length >= 35){
            return res.status(403).send('Picture upload limit reached (35 pictures)')
        }

        console.log(`Processing ${photos.length} images to ${filePath}`)

        // 2. Process with remove.bg and save the images to the appropriate UID location
        // Old API Key    'X-Api-Key': 'vWT8ncRMLQRe4EFpG8YCQoYk'
        // Old API Key    'X-Api-Key': 'hjD4fCSkcvqmU5hBoXptn4rb' 
        photos.forEach((photo, i) => {
          request.post({
            url: 'https://api.remove.bg/v1.0/removebg',
            formData: {
              image_file: photo.buffer,
              bg_image_file: background.buffer,
              size: 'full',
            },
            headers: {
              'X-Api-Key': 'vWT8ncRMLQRe4EFpG8YCQoYk'
            },
            encoding: null
          }, function(error, response, body) {
            if (error) return console.error('Request failed:', error);
            if (response.statusCode != 200) return console.error('Error:', response.statusCode, body.toString('utf8'));

            console.log('Photo #' + i + ' successfully processed with remove.bg');
            fs.writeFileSync(path.resolve(filePath, `${i.toString()}.png`), body);
          });
        });

        db.query(`UPDATE tokens SET tokens = tokens - ${photos.length} WHERE uid = ${jwt.decode(req.cookies.token).id}`);

        return res.status(200).send(`${photos.length} images uploaded successfully`)
    }catch(e){
        console.log(e);
    }
}

module.exports = add_images;
