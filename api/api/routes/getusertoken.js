// MySQL DB Connection
const db = require('../db')

// For decoding the user's id

const getUsertokens = (req, res) => {
  	try {	
  		if(req.params.uid){
	    	db.query(`SELECT * FROM tokens WHERE uid = ${req.params.uid}`, [], (err, rows) => {
		      if (err) throw err
	    		if(!rows.length){
					return res.status(400).send('User not found.')
		      	}
		      return res.status(200).json(rows[0])
		    });
	  	}else{
	  		return res.status(400).send('User not found.')
  		}
	} catch(err){
	  console.warn(err)
	  return res.status(400).send(err)
	}
}

module.exports = getUsertokens;
