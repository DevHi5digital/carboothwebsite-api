// MySQL DB Connection
const db = require('../db')

// For decoding the user's id
const jwt = require('jsonwebtoken')

const tokens = (req, res) => {
  try {
    db.query(`SELECT * FROM tokens WHERE uid = ${jwt.decode(req.cookies.token).id}`, [], (err, rows) => {
      if (err) throw err

      return res.status(200).send(rows[0])
    });
  } catch(err){
      console.warn(err)
      return res.status(400).send(err)
  }
}

module.exports = tokens;
